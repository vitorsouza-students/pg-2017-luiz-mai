# README #

Projeto de Graduação de Luiz Felipe Ferreira Mai: _Khoeus: uma plataforma de aprendizado focada no ensino da programação_.

### Resumo ###

Hoje, podemos encontrar diversas plataformas de aprendizado espalhadas pela Web que permitem que alunos e professores desenvolvam atividades relacionadas a um curso de maneira mais simplificada. No entanto, pode-se verificar uma série de barreiras quando o intuito é utilizá-las para cursos relacionados à Computação, uma vez que não dão suporte a códigos de programação e, por serem genéricos demais, estes sistemas possuem funcionalidades demais, apresentando uma interface complexa e não sendo bem aceito por parte dos alunos. Com o intuito de solucionar estes problemas apresentados acima, desenvolveu-se o sistema Khoeus que, por meio de funcionalidades code-oriented, busca apoiar diretamente o ensino da programação. 

Para a construção do sistema, seguiu-se um processo de Engenharia de Software realizando as etapas de levantamento de requisitos, especificação de requisitos, definição da arquitetura do sistema, implementação e testes. Foram colocadas em prática as disciplinas aprendidas no decorrer do curso, tais como Engenharia de Software, Engenharia de Requisitos, Projeto de Sistema de Software, Programação Orientada a Objetos e Desenvolvimento Web e Web Semântica. Também foram utilizados métodos e técnicas de modelagem e desenvolvimento como a técnica de Test Driven Development e o método FrameWeb para projeto de aplicações Web baseadas em frameworks.

### Atualizações ###

Este repositório contém uma cópia do original em https://github.com/luiz-mai/khoeus à época da conclusão do curso pelo aluno. Caso venha a fazer atualizações no repositório original, este poderá ficar defasado.