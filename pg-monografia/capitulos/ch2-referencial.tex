%==============================================================================
% TCC - Luiz Felipe Ferreira Mai
% Capítulo 2 - Referencial Teórico
%==============================================================================
\chapter{Referencial Teórico}
\label{sec-referencial}

Este capítulo apresenta os principais conceitos teóricos que fundamentaram o desenvolvimento do sistema \textit{Khoeus} e está organizado em 4 seções. A Seção~\ref{sec-engsoft} aborda a Engenharia de Software, destacando os principais conceitos e processos utilizados. A Seção~\ref{sec-webdev} apresenta os principais conceitos de desenvolvimento Web. A Seção~\ref{sec-tdd} fala sobre o método \textit{Test Driven Development} para automatizar os testes do sistema. A Seção~\ref{sec-frameweb} apresenta o método FrameWeb.



\section{Engenharia de Software}
\label{sec-engsoft}

O desenvolvimento de software é, sem dúvida, uma atividade que vem ganhando cada vez mais importância nos dias atuais pelo simples fato dos computadores estarem sendo utilizados cada vez mais nas mais diversas áreas do conhecimento humano. Visando melhorar a qualidade dos produtos de software e aumentar a produtividade no processo de desenvolvimento, surgiu a Engenharia de Software~\cite{falboEngSoft}.

Podemos dizer, de forma simplista, que um software consiste em:
\begin{enumerate}
	\item instruções que, quando executadas, fornecem características, funções e resultados desejados;
	\item estruturas de dados que possibilitam aos programas manipular informações adequadas;
	\item informação descritiva, tanto na forma impressa como na virtual, descrevendo a operação e o uso dos programas.
\end{enumerate}

Paralelamente a isso, pode-se dizer que a Engenharia de Software abrange um conjunto de práticas e um leque de ferramentas que possibilitam aos profissionais desenvolverem software de qualidade~\cite{pressman2011engenharia} por meio da especificação, desenvolvimento e manutenção destes sistemas de software. Tudo isso é feito por meio de tecnologias e práticas de gerência de projetos e outras disciplinas, visando organização, produtividade e qualidade nesse processo de desenvolvimento. A Engenharia de Software trata de aspectos relacionados ao estabelecimento de processos, métodos, técnicas, ferramentas e ambientes de suporte ao desenvolvimento de software~\cite{falboEngSoft}.


Embora um processo seja um conjunto de atividades, ações e tarefas realizadas na criação de algum produto, essa definição acaba se diferenciando no contexto da Engenharia de Software: aqui, um processo é uma abordagem adaptável que possibilita à equipe de software realizar o trabalho de escolher o conjunto apropriado de ações e tarefas~\cite{pressman2011engenharia}.

Como atividades de um processo de desenvolvimento de software, podemos citar a etapa de especificação e análise de requisitos e a etapa de projeto e implementação. Porém, vale ressaltar que paralelo a estas etapas principais, são realizados diversos processos de apoio e gerência, tais como elaboração de cronogramas, análise dos riscos etc.

Nas próximas seções, serão levantados alguns pontos referentes a este processo de desenvolvimento de software. Na Seção~\ref{sec-espec-anal-req}, será abordada a etapa de especificação e análise de requisitos. Na Seção~\ref{sec-proj-impl}, a etapa retratada será a de projeto e implementação. Em ambas, serão levantados alguns conceitos importantes para a uma melhor compreensão de sua importância para o processo de desenvolvimento de software como um todo.

\subsection{Especificação e Análise de Requisitos}
\label{sec-espec-anal-req}


A Engenharia de Requisitos é considerada uma das etapas mais cruciais no planejamento e no desenvolvimento de software, uma vez que lida com os problemas que surgem no planejamento do software correto para o cliente. Dessa forma, tem se tornado cada vez mais um processo que opera em diferentes níveis, incluindo aspectos relativos ao produto, ao projeto e à sua organização. O desenvolvimento da especificação de requisitos de um software é reconhecido como uma das bases das funcionalidades de um sistema. Estes requisitos são determinantes da qualidade do software,  dado que estudos empíricos mostraram que erros nos requisitos são as falhas mais comuns no ciclo de vida de um software, bem como as mais caras e custosas a corrigir~\cite{aurum2013managing}.

Por definição~\cite{ieee1990standard} um \textbf{requisito} é:
\begin{enumerate}
	\item Uma capacidade necessária do ponto de vista de um usuário para resolver um problema ou alcançar um objetivo;
	\item Uma condição que deve ser alcançada por um sistema ou componentes de um sistema a fim de satisfazer um contrato, norma, especificação ou outros documentos de formalização;
	\item Uma representação documentada de uma condição ou capacidade conforme (1) ou (2).
\end{enumerate}

Dito isto, podemos dizer que as atividade de especificação e análise de requisito não diz respeito apenas a perguntar às pessoas o que elas desejam, mas sim analisar cuidadosamente a organização, o domínio da aplicação e os processos de negócio no qual o sistema será utilizado~\cite{kotonya1998requirements}. Uma parte essencial dessa fase é a elaboração de modelos descrevendo o quê o software tem de fazer (e não como fazê-lo), dita Modelagem Conceitual. Até este momento, a ênfase está sobre o domínio do problema e não se deve pensar na solução computacional a ser adotada. Com os requisitos pelo menos parcialmente capturados e especificados na forma de modelos, pode-se começar a trabalhar no domínio da solução~\cite{falboEngReq}.

Pode-se dizer que os requisitos de um sistema incluem especificações dos serviços que o sistema deve prover, restrições sob as quais ele deve operar, propriedades gerais do sistema e restrições que devem ser satisfeitas no seu processo de desenvolvimento~\cite{falboEngReq}, sendo estes requisitos divididos em \textbf{requisitos funcionais} e \textbf{requisitos não funcionais}, definidos segundo~\cite{aurum2013managing} da seguinte forma:

\begin{itemize}
	\item \textbf{Requisitos Funcionais:} aquilo que o sistema fará em termos de tarefas e serviços;
	\item \textbf{Requisitos Não-funcionais:} restrições às soluções utilizadas para os requisitos funcionais, como desempenho e segurança.
\end{itemize}

Embora seja complexo, é possível dividir o processo de especificação e análise de requisitos em algumas etapas, conforme proposto por~\cite{kotonya1998requirements} e mostrado na Figura~\ref{fig-processo-req}.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figuras/referencial/processo-requisitos.png}
	\caption{Processo de Engenharia de Requisitos adaptado de~\cite{kotonya1998requirements}.}
	\label{fig-processo-req}
\end{figure} 

O processo começa pelo levantamento de requisitos, que deve levar em conta necessidades dos usuários e clientes, informações de domínio, sistemas existentes, regulamentos, leis etc. Uma vez identificados requisitos, é possível iniciar a atividade de análise, quando os requisitos levantados são usados como base para a modelagem do sistema. Tanto no levantamento quanto na análise de requisitos, é importante documentar requisitos e modelos. Para realizar essa documentação, dois documentos são normalmente utilizados: o Documento de Definição de Requisitos, contendo uma lista dos requisitos de cliente identificados, e o Documento de Especificação de Requisitos, que registra os requisitos de sistema e os vários diagramas resultantes do trabalho de análise. Os documentos produzidos são, então, verificados e validados. Adicionalmente, um esforço de garantia da qualidade deve ser realizado, visando garantir conformidade em relação a padrões e ao processo estabelecidos pela organização. Caso clientes, usuários e desenvolvedores estejam de acordo com os requisitos, o processo de desenvolvimento pode avançar; caso contrário, deve-se retornar à atividade correspondente para resolver os problemas identificados. Em paralelo a todas as atividades anteriormente mencionadas, há a gerência de requisitos, que se ocupa em gerenciar mudanças nos requisitos~\cite{falboEngReq}.

Dentre os modelos mais utilizados durante a etapa de especificação e análise de requisito, pode-se citar o modelo de casos de uso e o modelo conceitual estrutural. Enquanto o propósito do modelo de casos de uso é capturar e descrever a funcionalidade que um sistema deve prover, o modelo conceitual estrutural de um sistema tem por objetivo descrever as informações que devem ser representadas e gerenciadas~\cite{falboEngReq}.


\subsection{Projeto e Implementação}
\label{sec-proj-impl}

A fase de projeto tem por objetivo definir e especificar uma solução a ser implementada. É uma fase de tomada de decisão, tendo em vista que muitas soluções são possíveis. Além disso, o projeto é um processo de refinamento. Inicia-se com o projeto da arquitetura do sistema, que visa descrever a estrutura de nível mais alto da aplicação, identificando seus principais elementos ou componentes e como eles se relacionam uns com os outros. Uma vez definida a arquitetura, o projeto passa a se concentrar no detalhamento de cada um desses elementos, até atingir o nível de unidades de implementação~\cite{falboProjeto}, correspondendo à primeira atividade que leva em conta aspectos tecnológicos, sendo, portanto, a fase do processo de software na qual os requisitos, as necessidades do negócio e as considerações técnicas se juntam na formulação de um produto ou sistema de software~\cite{pressman2011engenharia}.

Inicialmente, o projeto é representado em um nível alto de abstração, enfocando a estrutura geral do sistema. Definida a arquitetura, o projeto passa a tratar do detalhamento de seus elementos. Esses refinamentos conduzem a representações de menores níveis de abstração, até se chegar ao projeto de algoritmos e estruturas de dados. Assim, independentemente do paradigma adotado, o processo de projeto envolve as seguintes atividades~\cite{falboProjeto}:

\begin{itemize}
	\item \textbf{Projeto da Arquitetura do Software:} visa definir os elementos estruturais do software e seus relacionamentos.
	\item \textbf{Projeto dos Elementos da Arquitetura:} visa projetar em um maior nível de detalhes cada um dos elementos estruturais definidos na arquitetura, o que envolve a decomposição de módulos em outros módulos menores.
	\item \textbf{Projeto Detalhado:} tem por objetivo refinar e detalhar os elementos mais básicos da arquitetura do software, i.e., as interfaces, os procedimentos e as estruturas de dados. Deve-se descrever como se dará a comunicação entre os elementos da arquitetura (interfaces internas), a comunicação do sistema em desenvolvimento com outros sistemas (interfaces externas) e com as pessoas que vão utilizá-lo (interface com o usuário), bem como se devem projetar detalhes de algoritmos e estruturas de dados. 
\end{itemize}

Ao final da fase de Projeto e Implementação, espera-se que a arquitetura do sistema esteja definida, bem como o projeto de seus componentes, divididos geralmente em camadas a fim de assegurar a separação de funcionalidades pertencentes a nichos diferentes. Embora não seja a única, uma das estruturas mais conhecida para o projeto de software é a que o divide em camada de apresentação, camada de domínio e camada de persistência~\cite{fowler2002patterns}. No caso da Web, na camada de \textbf{apresentação}, espera-se lidar com requisições HTTP, exibição de informação e iterações do usuário (como cliques de mouse, etc). Na camada de \textbf{domínio}, como o próprio nome já diz, o foco está nas variáveis do domínio do sistema, implementando todas as regras de negócio e requisitos do sistema. Já na camada de \textbf{persistência}, espera-se realizar todo o relacionamento da aplicação com o banco de dados, permitindo armazenar as informações do sistema e recuperá-las quando necessário.

\section{Desenvolvimento Web}
\label{sec-webdev}

O desenvolvimento de aplicações Web pode ser mais complexo e desafiador do que pode-se imaginar. Dois atributos-chave distinguem o desenvolvimento de softwares baseados na Web do desenvolvimento de softwares tradicionais: um rápido crescimento dos requisitos de sistemas Web e a mudança contínua de seus conteúdos. Dessa forma, aplicações Web precisam ser planejadas para serem escaláveis e de fácil manutenção, uma vez que tais recursos não podem ser adicionados posteriormente. O sucesso na construção, desenvolvimento, implementação e manutenção de um sistema Web depende fortemente de quão bem essas questões foram tratadas.

Com o avanço da Web, surgiu a chamada Engenharia Web, que lida com o processo de desenvolvimento de aplicações e sistemas Web. Sua essência é de gerenciar a diversidade e a complexidade dessas aplicações, evitando potenciais falhas que possam ocasionar implicações sérias. Pode-se dizer que a Engenharia Web é uma abordagem proativa de desenvolver aplicações Web~\cite{ginige2001web}.

\subsection{O Ambiente Web}
\label{sec-ambiente-web}

Para que as aplicações Web sejam executadas corretamente no computador do usuário, existem diversas tecnologias envolvidas neste processo e que mostram-se de suma importância para a total compreensão do problema. Desde o surgimento da Web, o projeto evoluiu e são várias as razões para o seu atual sucesso, mas duas merecem destaque: sua arquitetura simples (mas eficiente) e uma interface igualmente simples, originalmente baseada no paradigma de hipertextos.

A arquitetura por trás da Web é, basicamente, um cliente/servidor instalado sobre uma rede de computadores heterogênea. Do lado do cliente está um programa, chamado \textit{browser} ou navegador, que
intermedeia a solicitação de informações ao servidor e as apresenta para o usuário. Do outro lado, o servidor atende os diferentes clientes bem como outros servidores indistintamente. Esta arquitetura tem 3 componentes principais: o protocolo de comunicação
\textit{HyperText Transfer Protocol} (HTTP), o sistema de endereçamento \textit{Uniform Resource Locator} (URL) e a linguagem \textit{HyperText Markup Language} (HTML).

O \textbf{protocolo HTTP} é um meio de transporte de arquivos na Web que é executado sobre a camada TCP/IP da Internet. O protocolo consiste basicamente da transição de 4 estados: conexão (o cliente estabelece uma conexão com o servidor); requisição (o cliente envia um pedido ao servidor); resposta (o servidor devolve uma resposta ao cliente); e encerramento (a conexão é desfeita por ambas as partes). Quando um documento ou um objeto (como uma imagem, por exemplo) é enviado para o cliente, é anexado um cabeçalho com a informação necessária para que o \textit{browser} possa interpretá-lo e apresentá-lo adequadamente. Isto torna o protocolo independente do
\textit{browser}, que ignora o conteúdo de objetos cujo cabeçalho não compreende.

\textbf{URL} é a forma conhecida de endereçamentos de objetos na Web. Consiste na identificação do esquema utilizado (HTTP, FTP etc.) seguido do caminho até o objeto ou documento como, por exemplo: \url{http://www.ufes.br/index.html}.

O terceiro componente é a linguagem \textbf{HTML}, que especifica a estrutura e a formatação para documentos do tipo hipertexto por meio de \textit{tags} que indicam a forma como este deve ser visualizado. Pode-se destacar duas grandes vantagens do HTML: (1) a facilidade para associar informações por meio de links permitindo criar grandes redes de dados; e (2) o mecanismo de navegação uniforme com a simples seleção de objetos associados a links~\cite{winckler2002avaliaccao}.

\subsection{O padrão MVC de arquitetura de software}

Com o advento da Internet e da procura cada vez maior por sistemas Web, fez-se necessário criar formas de agilizar o desenvolvimento destes sem comprometer a qualidade do produto final. Com isso, surgiram vários padrões de arquitetura de software em busca de atributos de qualidade como: (1) decentralização do desenvolvimento por dividir o código da aplicação em vários componentes, possibilitando que os desenvolvedores possam trabalhar paralelamente em diferentes componentes sem que isso cause impactos no que foi desenvolvido por outra pessoa; e (2) fraco acoplamento, uma vez que um dos princípios destes padrões de arquitetura é que suas camadas sejam o mais isoladas possíveis a fim de que não interfiram nas funcionalidades de uma outra.

Dentre os diversos padrões propostos, um dos mais utilizados é o chamado MVC: \textit{Model-View-Controller}. Como o seu próprio nome já descreve, esse padrão busca a divisão da aplicação em três camadas, conforme mostra a Figura~\ref{fig-mvc}. Neste modelo, cada camada possui uma funcionalidade bem específica:

\begin{itemize}
	\item \textbf{Model:} consiste na camada que contém a lógica da aplicação, sendo responsável pelas regras de negócio e pela interação da aplicação com o banco de dados.
	\item \textbf{View:} basicamente, é a camada que interage diretamente com o usuário, exibindo dados por meio de arquivos HTML, XHTML, PDF, entre outros. No caso do \textit{Ruby on Rails}, a camada de visão utiliza arquivos HTML contendo códigos Ruby embutidos.
	\item \textbf{Controller:} pode-se dizer que essa camada é a intermediadora do sistema, comunicando-se com as outras duas camadas. As requisições provenientes do \textit{browser} são processadas pelo \textit{controller}, que processa as informações dos \textit{models} e as retorna para as \textit{views} para que sejam exibidas ao usuário. 
\end{itemize}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{figuras/referencial/modelo_mvc.png}
	\caption{Representação do modelo de arquitetura de software \textit{Model-View-Controller}~\cite{wikipedia-mvc}}
	\label{fig-mvc}
\end{figure} 

\subsection{Ruby e seu framework \textit{Ruby on Rails}}
\label{sec-ruby-rails}

Ruby é uma linguagem moderna, \textit{open source} e orientada a objetos: tudo que pode ser manipulado é um objeto, assim como o resultado dessas manipulações. Além disso, Ruby consegue ser concisa sem ser ininteligível: é possível expressar ideias naturalmente e de forma clara com códigos desenvolvidos em Ruby. Isso acarreta em programas que são fáceis de escrever e fáceis de interpretar meses após sua escrita. 

Para o desenvolvimento Web, o Ruby oferece o \textit{framework} \textit{Ruby on Rails} (RoR) que facilita o processo de codificação, implementação e manutenção das aplicações. Pode-se dizer que existem duas filosofias por trás do RoR: ``\textit{Don't Repeat Yourself (DRY)}'' e ``\textit{Convention over Configuration}''. Na primeira, a ideia é que um pedaço de código deve ser utilizado apenas em um lugar, evitando repetição desnecessária. Na segunda, utiliza-se uma série de convenções adotadas pelo \textit{framework} a fim de reduzir a quantidade de código utilizada~\cite{ruby2013agile}.

A arquitetura do \textit{Ruby on Rails} é dividida em módulos que servem a funcionalidades distintas dentro do contexto da aplicação. Para o padrão MVC, são utilizados três módulos que servem a cada uma das camadas definidas acima, como mostra a Figura~\ref{figura-arq-rails}: para as \textit{views}, utiliza-se o módulo \textbf{\textit{ActionView}}, que provê funcionalidades para a exibição das páginas, construção de formulários, etc. Para os \textit{models}, o módulo \textbf{\textit{ActiveRecord}} garante a associação entre as classes do sistema, bem como o relacionamento da aplicação com o banco de dados. Por fim, mas não menos importante, os \textit{controllers} pertencem ao módulo \textbf{\textit{ActionController}} (ou simplesmente \textit{Controller}), o qual provê um ambiente intermediário entre os \textit{models} e as \textit{views}, garantindo que a informação correta seja exibida ao usuário.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figuras/referencial/arquitetura_rails.png}
	\caption{Arquitetura do Ruby on Rails~\cite{adrian-mejia}.}
	\label{figura-arq-rails}
\end{figure}

\section{Test Driven Development (TDD)}
\label{sec-tdd}
A utilização de \textit{Test Driven Development} se tornou uma prática comum na comunidade de desenvolvimento de software. A técnica consiste em escrever testes antes de desenvolver o produto propriamente dito, agregando muitos benefícios ao processo de desenvolvimento. O primeiro deles, e mais claro, são os benefícios na qualidade externa do produto, dando mais segurança ao desenvolvedor na hora de mudanças. 

Os testes automatizados, que rodam em questão de segundos, são executados o tempo todo pelo desenvolvedor. Ao contrário dos testes manuais, caso algo pare de funcionar, o desenvolvedor é rapidamente notificado e consegue corrigir o problema de imediato. Além disso, pode-se dizer que um desenvolvedor, ao praticar TDD, divide seu trabalho em pequenas etapas: escreve-se um pequeno teste, implementa-se um pedaço da funcionalidade e esse ciclo se repete até o fim do desenvolvimento. A cada teste escrito (e executado), o desenvolvedor ganha \textit{feedback} dos erros do sistema e, além disso, ganha um ponto de partida para a análise do erro e sua possível solução, como pode ser visualizado na Figura~\ref{fig-tdd}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{figuras/referencial/tdd.png}
	\caption{Comparação da abordagem tradicional de desenvolvimento de software com o desenvolvimento guiado por testes~\cite{tdd-caelum}}
	\label{fig-tdd}
\end{figure} 

Sempre que um desenvolvedor pega uma funcionalidade para fazer, ele a quebra em pequenas tarefas. Tais tarefas exigem a escrita de código: classes são criadas enquanto outras são modificadas. Ao praticar TDD, o desenvolvedor antes de começar a fazer essas modificações explicita esses objetivos por meio de testes automatizados. O teste em código nada mais é do que um trecho de código que deixa claro o que determinado trecho de código deve fazer. Ao formalizar esse objetivo na forma de um teste automatizado, esse teste falha, claro; afinal, a funcionalidade ainda não foi implementada. O desenvolvedor então trabalha para fazer esse teste passar implementando a funcionalidade da forma que julgar necessária~\cite{tdd-caelum}.



\section{O método FrameWeb}
\label{sec-frameweb}

FrameWeb é um método de projeto para construção de sistemas de informação Web (Web Information Systems – WISs) baseado em \textit{frameworks}. O método assume que determinados tipos de frameworks serão utilizados durante a construção da aplicação, define uma arquitetura básica para o WIS e propõe modelos de projeto que se aproximam da implementação do sistema usando esses \textit{frameworks}~\cite{souza-frameweb07}.

O FrameWeb define uma arquitetura lógica padrão para WISs baseada no padrão arquitetônico \textit{Service Layer} (Camada de Serviço)~\cite{fowler2002patterns}. O sistema é dividido em três grandes camadas~\cite{salvatore2016alocaweb}:

\begin{itemize}
	\item\textbf{ Camada de Negócio \textit{(Business Tier)}:} responsável pelas funcionalidades relacionadas
	às regras de negócio da aplicação. Esta camada é particionada em duas: Lógica de	Domínio (\textit{Domain}) e Lógica de Aplicação (\textit{Application});
	\item \textbf{Camada de Apresentação \textit{(Presentation Tier)}:} responsável por funcionalidades de interface com o usuário (incluindo as telas que serão apresentadas a ele). Esta camada, segundo o padrão proposto, é também particionada em duas outras: Visão (\textit{View}) e Controle (\textit{Control});
	\item \textbf{Camada de Acesso a Dados \textit{(Data Access Tier)}:} responsável por funcionalidades
	relacionadas à persistência de dados.
\end{itemize}

A fase de Projeto concentra as propostas principais do método: (i) definição de uma arquitetura padrão que divide o sistema em camadas, de modo a se integrar bem com os \textit{frameworks} utilizados; (ii) proposta de um conjunto de modelos de projeto que trazem conceitos utilizados pelos \textit{frameworks} para esta fase do processo por meio da definição de uma linguagem específica de domínio que faz com que os diagramas fiquem mais próximos da implementação~\cite{souza-frameweb07,martins-mestrado15}.

Para representar componentes típicos da plataforma Web e dos \textit{frameworks} utilizados, o FrameWeb estende o meta-modelo da UML, especificando, assim, uma sintaxe própria. Com isso, é possível utilizá-lo para a construção de diagramas de quatro tipos:

\begin{itemize}
	\item \textbf{Modelo de Entidades:} é um diagrama de classes da UML que representa os objetos de	domínio do problema e seu mapeamento para a persistência em banco de dados relacional;
	\item \textbf{Modelo de Persistência:} é um diagrama de classes da UML que representa as classes DAO existentes, responsáveis pela persistência das instâncias das classes de domínio.
	\item \textbf{Modelo de Navegação:} é um diagrama de classe da UML que representa os diferentes componentes que formam a camada de Lógica de Apresentação, como páginas Web, formulários HTML e \textit{controllers}.
	\item \textbf{Modelo de Aplicação:} é um diagrama de classes da UML que representa as classes de	serviço, que são responsáveis pela codificação dos casos de uso, e suas dependências.
\end{itemize}