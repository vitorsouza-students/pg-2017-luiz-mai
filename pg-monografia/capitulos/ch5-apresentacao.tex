% ==============================================================================
% TCC - Luiz Felipe Ferreira Mai
% Capítulo  - Apresentação do Sistema
% ==============================================================================

\chapter{Implementação e Apresentação}
\label{sec-apresentacao}

Neste capítulo serão apresentados aspectos da implementação do sistema, bem como seu resultado final por meio de diversas capturas de tela. 

\section{Implementação do sistema}
Conforme mencionado na Seção~\ref{sec-projeto-arquitetura-sistema}, o Khoeus foi desenvolvido utilizando o padrão MVCS e o framework \textit{Ruby on Rails}. Dessa forma, os diversos diretórios adotados foram organizados a fim de favorecer a navegação entre os vários arquivos conforme a Figura~\ref{fig-diretorios-rails}. Dentre os diversos diretórios listados, destacam-se:

\begin{itemize}
	\item \textbf{/app:} inclui a maioria dos arquivos específicos da aplicação
		\begin{itemize}
			\item \textbf{/controllers:} contém todos os arquivos da camada \textit{Controller} do sistema.
			\item \textbf{/models:} contém todos os arquivos da camada \textit{Model} do sistema.
			\item \textbf{/services:} contém todos os arquivos da camada \textit{Service} do sistema.
			\item \textbf{/views:} contém todos os arquivos da camada \textit{View} do sistema.
		\end{itemize}
	\item \textbf{/config:} contém arquivos de configuração de servidor, bancos de dados, email e rotas.
	\item \textbf{/db:} contém os arquivos chamados \textit{migrations} que são responsáveis pela criação/edição de tabelas do banco de dados.
	\item \textbf{/spec:} inclui os arquivos de testes utilizados pelo padrão TDD, exposto na Seção~\ref{sec-tdd}.
\end{itemize}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.3\textwidth]{figuras/apresentacao/diretorios.png}
	\caption{Estrutura de diretórios utilizada no sistema}
	\label{fig-diretorios-rails}
\end{figure} 

Nessa seção, serão levados em consideração apenas os diretórios relacionados diretamente ao padrão MVCS, ou seja: os diretórios \textbf{/controllers}, \textbf{/models}, \textbf{/services} e \textbf{/views}.

\subsection{Model}
\label{sec-model}

Neste diretório, o Rails armazena as informações de cada classe específica do sistema, como \textbf{Classroom} ou \textbf{User}, sendo que cada uma delas possui um arquivo exclusivo contendo informações relativas a validação de dados, associações e herança. Vale ressaltar que os atributos de cada classe são definidos pelas \textit{migrations}, e não pelos \textit{models}.

A título de exemplo, o Código~\ref{lst:model} mostra algumas das propriedades possíveis para um \textit{Model} do sistema. Para melhor exemplificar a validação de atributos, as associações entre classes e até mesmo o mecanismo de herança, escolheu-se a classe \texttt{\textbf{BoardItem}}. 

\begin{lstlisting}[
	language=Ruby,
	label=lst:model,
	caption={\textit{Model} da classe BoardItem}]
class BoardItem < ApplicationRecord
	belongs_to :section

	has_attached_file :document_file

	self.inheritance_column = :type

	validates :title,
		presence: true
	validates :position,
		presence: true,
		numericality: {only_integer: true}
end
\end{lstlisting}

Para as associações, o \textit{Ruby on Rails} provê os métodos \texttt{\textbf{belongs\_to}}, \texttt{\textbf{has\_one}} e \texttt{\textbf{has\_many}} de forma a especificar quais são as outras classes com que aquela se relaciona, bem com especificando a cardinalidade dessa associação.

Para o caso de herança, o \textit{Ruby on Rails} implementa por padrão o modelo de herança baseado em \textit{Single Table Inheritance}, ou seja, o banco de dados possui apenas uma tabela contendo os atributos de todas as classes especializadas. Para permitir isso, deve-se informar qual será a coluna que irá diferenciar a classe em questão por meio do atributo \texttt{\textbf{inheritance\_column}}.

A fim de permitir que certos parâmetros sejam validados no momento de sua criação ou edição, é possível especificar quais serão as validações para cada um dos atributos da classe. No caso da classe \texttt{\textbf{BoardItem}}, tanto o atributo \texttt{title} quanto o atributo \texttt{position} devem estar obrigatoriamente presentes. No caso deste último, deve-se garantir que ele é um inteiro.

\subsection{Controller}
\label{sec-controller}

A fim de controlar as requisições realizadas pelos usuários e seus respectivos retornos, o sistema utiliza de classes controladoras de forma a modularizar o tratamento dado a cada uma delas. Por meio do arquivo de rotas (não detalhado neste capítulo a fim de simplificação), o \textit{Ruby on Rails} garante que, ao realizar uma requisição para uma determinada URL, um determinado método do controlador responsável será executado.

Sempre que um determinado usuário realiza uma requisição a uma determinada rota do sistema, o arquivo de rotas (Código~\ref{lst:rotas}) encontra o método do controlador responsável por aquela requisição e o executa. Com relação aos casos de uso CRUD para as turmas do sistema, todas as requisições são gerenciadas pela classe \texttt{ClassroomsController}, conforme Código~\ref{lst:controller}. Como é possível observar, o \textit{Controller} realiza o processo de autenticação e autorização das rotas por meio do método \texttt{load\_and\_authorize\_resource} e carrega a turma (\texttt{Classroom}) em questão no caso dos métodos \texttt{show}, \texttt{edit}, \texttt{update} e \texttt{destroy}, uma vez que estes se tratam de uma turma já criada anteriormente.

\begin{lstlisting}[
language=Ruby,
label=lst:rotas,
caption={Gerenciamento das rotas do sistema}]
Rails.application.routes.draw do

resources :users
resources :classrooms 
# Other routes and resources

end
\end{lstlisting}

\begin{lstlisting}[
language=Ruby,
label=lst:controller,
caption={\textit{Controller} responsável pela inscrição de usuários em turmas (versão simplificada)}]
class ClassroomsController < ApplicationController	
	load_and_authorize_resource
	
	before_action :set_classroom, only: [:show, :edit, :update, :destroy]
	
	# GET /classrooms
	def index
		@classrooms = Classroom.all
	end
	
	# GET /classrooms/1
	def show
		@sections = ManageSectionService.new.retrieve_from_classroom(@classroom)
		@items = ManageBoardItemService.new.retrieve_from_classroom(@classroom)
	end
	
	# GET /classrooms/new
	def new
		@classroom = Classroom.new
	end
	
	# GET /classrooms/1/edit
	def edit
	end
	
	# POST /classrooms
	def create
		if (@classroom = ManageClassroomService.new.create(classroom_params))
			redirect_to @classroom, notice: 'Classroom was successfully created.'
		else
			render :new
		end
	end
	
	# PATCH/PUT /classrooms/1
	def update
		if ManageClassroomService.new(@classroom).update(classroom_params)
			redirect_to @classroom, notice: 'Classroom was successfully updated.'
		else
			render :edit
		end
	end
	
	# DELETE /classrooms/1
	def destroy
		ManageClassroomService.new(@classroom).delete
		redirect_to classrooms_url, notice: 'Classroom was successfully destroyed.'
	end
	
	private
	def set_classroom
		@classroom = ManageClassroomService.new.retrieve(params[:id]) || ManageClassroomService.new.retrieve(params[:classroom_id])
	end
	
	def classroom_params
		params.require(:classroom).permit(:name, :password, :has_grades, :ha_attendance, :minimum_grade )
	end

end

\end{lstlisting}

A partir disso, cada um dos métodos públicos do controlador irá cuidar da requisição pela qual ele é necessário. No caso do método \texttt{create}, por exemplo, o serviço \texttt{ManageClassroomService} (explicado mais detalhadamente na Seção~\ref{sec-services}) é invocado numa tentativa de criar uma nova instância de turma. Caso seja bem sucedido, o usuário é redirecionado para a página da turma e notificado de que a turma foi criada com sucesso. Caso contrário, ele retorna ao formulário de criação para que possa corrigir seus erros.




\subsection{View}
\label{sec-view}

Na camada \textit{View} do sistema são gerenciados todos os arquivos relacionados à exibição de informações aos usuários como páginas HTML, formulários e \textit{partials} (conteúdo HTML reaproveitável em múltiplas páginas). Para permitir a exibição de conteúdo dinâmico, é possível embutir código Ruby em todos estes arquivos HTML. Para isso, o \textit{framework} utilizado permite que todas as variáveis com um \texttt{@} no começo definidas no controlador possam ser acessadas nos arquivos da camada de visão relacionados àquele \textit{controller}. Como exemplo, pode-se citar a possibilidade de iterar sobre uma lista de turmas e acessar o título de cada uma delas, conforme mostrado no Código~\ref{lst:view}.

\begin{lstlisting}[
label=lst:view,
caption={Exibição das turmas do sistema}]
<div class="row">
	<% @classrooms.each do |classroom| %>
		<!-- exibição de informações da turma -->
		<p class="number text-center"><%= students_count(classroom) %></p>
		<h2 class="classroom-title"><%= classroom.name %></h2>
	<% end %>
</div>
\end{lstlisting}


\subsection{Services}
\label{sec-services}

Por padrão, o \textit{Ruby on Rails} não fornece uma camada de serviço e, portanto, espera-se que todas as funcionalidades do sistema sejam tratadas diretamente na camada \textit{Model}. No entanto, conforme proposto por~\cite{benlewis-drycontrollers}, a utilização de uma Camada de Serviço dentro do \textit{framework} torna-se um adicional de grande valor uma vez que facilita a organização dos casos de uso do sistema: ao invés de cada classe do sistema tratar um pedaço de diversos casos de uso, estes são concentrados em classes específicas.

A fim de facilitar os métodos de CRUD para as diversas classes do sistema, foi criada uma classe de serviço genérica chamada \texttt{CrudService} contendo os cinco métodos principais: \texttt{list}, \texttt{retrieve}, \texttt{create}, \texttt{update} e \texttt{delete} conforme Código~\ref{lst:crudservice}. A fim de permitir que todas as outras classes de serviço possam utilizar esses métodos, a classe \texttt{CrudService} inicializa em seu construtor as variáveis que definirão a classe e a instância em questão (caso exista). Com isso, todas as classes de serviço herdam de \texttt{CrudService} os métodos básicos de CRUD e podem implementar outros caso seja necessário.

\begin{lstlisting}[
language=Ruby,
label=lst:crudservice,
caption={Classe genérica de serviço contendo os principais métodos de CRUD}]
class CrudService

	def initialize(model, entity = nil)
		@model = model.classify.constantize
		@entity = entity if entity
	end
	
	def list
		@model.all
	end
	
	def create(params)
		@entity = @model.create(params)
		@entity.save ? @entity : false
	end
	
	def retrieve(id)
		@entity = @model.find_by(id: id)
	end
	
	def update(params)
		@entity.update_attributes(params)
	end
	
	def delete
		@entity.destroy
	end
	
end
\end{lstlisting}

\subsection{Test-Driven Development}
\label{sec-tdd-impl}

Como já abordado na Seção~\ref{sec-tdd}, usou-se do método de programação orientada a testes a fim de garantir a consistência da plataforma. Para isso, utilizou-se o framework \textit{Rspec} que provê uma série de classes que permitem realizar os testes automatizados no sistema.

Conforme Código~\ref{lst:spec-classroomcontroller}, pode-se observar que os testes relacionados aos \textit{controllers} do sistema simulam acessos a certas páginas assumindo determinadas \textit{roles} e, com isso, espera-se uma certa resposta (que pode ser um redirecionamento, a exibição de um erro ou de uma determinada página). No caso das ações de \textit{create} para um CRUD, fica nítido que espera-se a criação de uma nova entidade do sistema.

\begin{lstlisting}[
language=Ruby,
label=lst:spec-classroomcontroller,
caption={Trecho do código responsável por testar a classe \textit{ClassroomController}}]
RSpec.describe ClassroomsController, type: :controller do
	let(:user) {create :user, confirmed: true}
	let(:other_user) {create :user, email: 'aaa@gmail.com', confirmed: true}
	let(:admin_user) {create :admin_user}
	let(:classroom) {create :classroom}
	let(:subscription) {create :subscription}
	let(:teacher_subscription) {create :teacher_subscription}
	
	describe 'GET #new' do
		context 'when admin' do
			it 'returns a success response' do
				log_in admin_user
				get :new
				expect(response).to have_http_status 200
			end
		end
		context 'when not admin' do
			it 'returns status 403' do
				log_in user
				get :new
				expect(response).to have_http_status 403
			end
		end
		context 'when logged out' do
			it 'redirect to login page' do
				get :new
				expect(response).to redirect_to login_path
			end
		end
	end
	
	
	describe 'POST #create' do
		context 'when not admin' do
			it 'returns status 403' do
				log_in user
				post :create, params: {classroom: attributes_for(:classroom)}
				expect(response).to have_http_status 403
			end
		end
		context 'when admin with invalid params' do
			it 'redirects to create page' do
				log_in admin_user
				post :create, params: {classroom: attributes_for(:classroom, name: nil)}
				expect(response).to render_template :new
			end
		end
		context 'when admin with valid params' do
			before(:each) {log_in admin_user}
			it 'creates a new Classroom' do
				expect {
				post :create, params: {classroom: attributes_for(:classroom)}
				}.to change(Classroom, :count).by(1)
			end
			
			it 'redirects to the created classroom' do
				post :create, params: {classroom: attributes_for(:classroom)}
				expect(response).to redirect_to(Classroom.last)
			end
		end
	end
	
	#Other tests
end
\end{lstlisting}

Além dos testes orientados aos \textit{controller}, também foram criados testes voltados à validação dos atributos das entidades e alguns testes focados na funcionalidade de algumas classes de serviço, como a resolução de uma prova.


\section{Apresentação do sistema}
\label{sec-apresentacao-sistema}

Nesta seção, apresentar-se-á o sistema por meio de uma série de capturas de tela. A Figura~\ref{fig-pagina-inicial} mostra a tela inicial de login no sistema onde o usuário poderá escolher entre a opção de se cadastrar ou, caso já possua um cadastro, efetuar login.

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/tela-inicial.png}
	\caption{Tela inicial}
	\label{fig-pagina-inicial}
\end{figure}

Caso o usuário queira se cadastrar, ele será levado à tela de cadastro (Figura~\ref{fig-cadastro}) onde deverá preencher suas informações como nome, e-mail e senha desejada. Aqui, vale ressaltar que uma série de validações é feita, como por exemplo a validação do formato do e-mail, da presença do nome e do tipo de dado inserido no campo Número (que deve aceitar apenas inteiros). Uma vez realizado o cadastro, o usuário receberá em sua caixa de entrada um e-mail contendo o link de ativação para que possa acessar o sistema.

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/tela-cadastro.png}
	\caption{Tela de cadastro}
	\label{fig-cadastro}
\end{figure}

Assim que a conta do usuário estiver ativada, ele poderá acessar a tela de login (Figura~\ref{fig-login}) para acessar o sistema por meio de seu e-mail e senha escolhidos durante o cadastro. Caso deseje permanecer logado mesmo após fechar o navegador, o usuário poderá marcar a opção \textit{Lembrar de mim}. Caso tenha esquecido sua senha, o usuário poderá solicitá-la por meio do link \textit{Forgot your password?} ainda na tela de login. Uma vez solicitada, o usuário receberá em sua caixa de entrada um e-mail com um link que dá direito a realizar o \textit{reset} de sua senha.

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/tela-login.png}
	\caption{Tela de login}
	\label{fig-login}
\end{figure}

Após finalmente logar no sistema, o usuário terá acesso a uma lista de turmas, conforme mostra a figura~\ref{fig-turmas}. Caso o usuário seja um administrador do sistema, ele poderá criar novas turmas (Figura~\ref{fig-criar-turma}) ou acessar qualquer um das turmas existentes no sistema. Vale ressaltar que no momento da criação da turma é importante especificar a forma com que a média será calculada por meio da definição das categorias de nota e seus respectivos pesos. Caso o usuário não seja um administrador, ele deverá se inscrever na turma desejada por meio de uma senha que deverá ser fornecida a ele previamente.

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/lista-turmas.png}
	\caption{Lista de turmas cadastradas no sistema}
	\label{fig-turmas}
\end{figure}


\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/form-criar-turma.png}
	\caption{Formulário de criação de turma}
	\label{fig-criar-turma}
\end{figure}

Uma vez inscrito em uma turma, o usuário terá acesso ao \textit{board} daquela turma, bem como ao seu livro de notas e à lista de presença. 

No \textit{board}, o aluno terá acesso a todo o conteúdo publicado pelos seus professores divididos em seções, conforme a Figura~\ref{fig-board}. Cada seção pode conter links, documentos, questionários, provas, tarefas ou atividades externas. Caso o usuário seja um professor da turma, ele poderá adicionar novos itens ao \textit{board}, cada um deles com seu próprio formulário e seus próprios atributos. No caso de provas, tarefas e atividades externas, vale ressaltar que o professor deverá escolher a qual categoria de nota aquele item pertence. Isso será essencial no momento do cálculo da média dos alunos. Caso o usuário seja apenas um aluno, ele poderá interagir de forma diferente com cada um dos tipos de item disponíveis.

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/board-turma.png}
	\caption{Board de uma determinada turma}
	\label{fig-board}
\end{figure}

Caso o aluno clique sobre um \textbf{Link}, uma nova aba abrirá no navegador levando o usuário para a página em questão. Caso clique sobre um \textbf{Arquivo}, será dado início ao \textit{download} daquele documento em questão. No caso de \textbf{Questionários}, o aluno poderá responder a uma série de perguntas objetivas mas que não possuem caráter avaliativo, conforme mostra a Figura~\ref{fig-questionario}. Caso o aluno já tenha respondido ao questionário, não poderá fazê-lo novamente. Caso o usuário seja um professor da turma, ao clicar em um questionário ele poderá conferir as estatísticas das respostas que foram dadas até o momento, conforme Figura~\ref{fig-estatisticas-questionario}.

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/tela-questionario.png}
	\caption{Tela para que o aluno responda a um questionário}
	\label{fig-questionario}
\end{figure}


\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/tela-resultado-questionario.png}
	\caption{Tela com as estatísticas de um questionário}
	\label{fig-estatisticas-questionario}
\end{figure}

Ao clicar sobre uma \textbf{Prova}, o aluno será levado a uma tela contendo as diversas questões de uma prova, as quais deverá resolvê-la, conforme. Cada prova é composta por questão objetivas e/ou discursivas, sendo que cada uma delas possui um valor definido pelo professor no momento da criação. Caso o aluno já tenha resolvido a prova, ao clicar sobre o item do \textit{board} ele será levado para a tela contendo o \textit{feedback} daquela prova (caso já tenha sido avaliada), conforme mostra a Figura~\ref{fig-feedback-prova}. Caso o usuário seja um professor da turma, ele será levado a uma tela com a listagem de todos os alunos onde poderá escolher qual aluno deseja avaliar (Figura~\ref{fig-alunos-prova}), sendo levado a uma tela onde poderá corrigir cada uma das respostas do aluno (no caso de questões discursivas), conforme mostra a Figura~\ref{fig-avaliar-prova}.

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/tela-resolver-prova.png}
	\caption{Tela onde um aluno poderá resolver uma prova}
	\label{fig-prova}
\end{figure}

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/tela-feedback-prova.png}
	\caption{Tela onde o aluno poderá consultar sua nota e comentários do professor}
	\label{fig-feedback-prova}
\end{figure}


\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/tela-lista-alunos.png}
	\caption{Tela onde o professor poderá ver a listagem de todos os alunos}
	\label{fig-alunos-prova}
\end{figure}



\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/tela-avaliar-prova.png}
	\caption{Tela onde o professor poderá avaliar a prova de um aluno}
	\label{fig-avaliar-prova}
\end{figure}

No caso de \textbf{Tarefas}, o aluno poderá se deparar com três tipos diferentes de submissão:
\begin{itemize}
	\item \textbf{Texto:} o aluno deverá escrever um texto que responda o que foi solicitado na tarefa, conforme Figura~\ref{fig-tarefa-texto}.
	\item \textbf{Arquivo:} o aluno deverá enviar um arquivo contendo o conteúdo solicitado na tarefa, conforme Figura~\ref{fig-tarefa-arquivo}.
	\item \textbf{Código:} o aluno poderá executar seu código antes de enviá-lo, conforme Figura~\ref{fig-tarefa-codigo}.
\end{itemize}

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/submissao-texto.png}
	\caption{Submissão de tarefas de texto}
	\label{fig-tarefa-texto}
\end{figure}

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/submissao-arquivo.png}
	\caption{Submissão de tarefas de arquivo}
	\label{fig-tarefa-arquivo}
\end{figure}

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/submissao-codigo.png}
	\caption{Execução do código antes de submetê-lo}
	\label{fig-tarefa-codigo}
\end{figure}

Caso o usuário seja um professor da turma, ele será levado a uma tela com a listagem de todos os alunos similar à mostrada na Figura~\ref{fig-alunos-prova} onde poderá escolher qual aluno deseja avaliar, sendo levado a uma tela onde poderá atribuir uma nota e um \textit{feedback} à submissão do aluno. No caso de tarefas de código, o professor poderá deixar um comentário em uma linha de código específica, caso julgue necessário (Figura~\ref{fig-avaliar-codigo}). Uma vez avaliada a submissão de um aluno, ele poderá consultá-la pela página da tarefa, onde verá a sua nota e o comentário deixado pelo professor. No caso de tarefas de código, o aluno também poderá consultar os comentários feitos em linhas de códigos específicas, conforme a Figura~\ref{fig-avaliacao-codigo}.

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/avaliacao-codigo.png}
	\caption{Avaliação de tarefas de código}
	\label{fig-avaliar-codigo}
\end{figure}

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/feedback-codigo.png}
	\caption{Feedback atribuído a linhas de código individualmente}
	\label{fig-avaliacao-codigo}
\end{figure}

Por fim, a interação com as atividades externas é muito mais simples uma vez que assume-se que ela já foi realizada externamente ao sistema. Por isso, professores podem apenas atribuir uma nota aos alunos da turma (Figura~\ref{fig-avaliacao-externa}) e alunos podem apenas consultar sua nota (Figura~\ref{fig-feedback-externa}).

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/avaliacao-externa.png}
	\caption{Tela para avaliar atividades externas de um aluno}
	\label{fig-avaliacao-externa}
\end{figure}

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/feedback-externa.png}
	\caption{Tela com a nota e o feedback de uma atividade externa}
	\label{fig-feedback-externa}
\end{figure}

Ao longo do curso, os alunos poderão consultar seu Livro de Notas (Figura~\ref{fig-livro-notas}) onde são exibidas as notas em cada uma das atividades avaliativas que foram realizadas ao longo do curso.

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/livro-notas.png}
	\caption{Livro de notas}
	\label{fig-livro-notas}
\end{figure}

Para permitir que os alunos acompanhem melhor o curso, um professor poderá adicionar novas aulas ao calendário de aulas (Figura~\ref{fig-calendario}) por meio de um formulário onde deverá atribuir uma data para aquela aula.

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/calendario-aulas.png}
	\caption{Calendário de aulas}
	\label{fig-calendario}
\end{figure}

Com base nas aulas definidas, um professor poderá atribuir presença ou ausência aos alunos de uma turma em cada uma das aulas por meio de um formulário conforme mostra a Figura~\ref{fig-presenca}. Ao longo do curso, os alunos poderão consultar a lista de presença (incluindo a porcentagem de aulas em que esteve presente), conforme a Figura~\ref{fig-lista-presenca}.

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/atribuir-presenca.png}
	\caption{Formulário para a atribuição de presença/ausência em uma determinada aula}
	\label{fig-presenca}
\end{figure}

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{figuras/apresentacao/lista-presenca.png}
	\caption{Lista de presença de uma turma}
	\label{fig-lista-presenca}
\end{figure}