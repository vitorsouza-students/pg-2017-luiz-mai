% ==============================================================================
% TCC - Luiz Felipe Ferreira Mai
% Capítulo 4 - Projeto Arquitetural
% ==============================================================================

\chapter{Projeto Arquitetural}
\label{sec-projeto}


Como proposto na Seção~\ref{sec-engsoft}, após as fases de especificação e análise de requisitos, ocorre a fase de projeto do sistema. Esta etapa é responsável pela modelagem de como será a implementação do sistema, incorporando, aos requisitos, as tecnologias a serem utilizadas. 

Neste capítulo iremos mostrar a arquitetura do sistema. Na Seção~\ref{sec-projeto-arquitetura-sistema}, a arquitetura do sistema é descrita, enquanto na Seção~\ref{sec-projeto-modelos-frame-web} os modelos FrameWeb são apresentados.






% ========================================================================================
%   Arquitetura do Sistema
% ========================================================================================

\section{Arquitetura do Sistema}
\label{sec-projeto-arquitetura-sistema}

A arquitetura de software do sistema Khoeus baseia-se no padrão MVCS (\textit{Model, View, Controller, Services}) em uma tentativa de separar a representação de suas informações da interação realizada pelo usuário. 
A Figura~\ref{figura-mvcs} retrata o relacionamento entre as camadas deste padrão. Vale notar que as linhas sólidas representam relacionamento direto, enquanto as linhas tracejadas representam relacionamento indireto. Dessa forma, temos que:

\begin{itemize}
	\item \textbf{Model:} consiste na camada responsável pelas regras de negócio e pela interação da aplicação com o banco de dados.
	\item \textbf{View:} basicamente, é a camada que interage diretamente com o usuário, exibindo dados por meio de arquivos HTML, XHTML, PDF, entre outros. No caso do \textit{Ruby on Rails}, a camada de visão utiliza arquivos HTML contendo código Ruby embutidos.
	\item \textbf{Controller:} pode-se dizer que essa camada é a intermediadora do sistema, comunicando-se com as outras três camadas. As requisições provenientes do \textit{browser} são processadas pelo \textit{controller}, que processa as informações dos \textit{models}, chama os serviços necessários e as retorna para as \textit{views} para que sejam exibidas ao usuário. 
	\item \textbf{Services:} nesta camada, espera-se agrupar os casos de uso desejados de forma a desacoplar as funcionalidades do sistema dos \textit{controllers} ou dos \textit{models}. Além disso, espera-se obter o máximo de reusabilidade dos serviços.
\end{itemize}

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.6\textwidth]{figuras/projeto/modelo_mvcs.png}
	\caption{Representação do padrão de design MVCS.}
	\label{figura-mvcs}
\end{figure}

Falando mais especificamente do \textit{Ruby on Rails}, temos que sua arquitetura é dividida em módulos que servem a funcionalidades distintas dentro do contexto da aplicação. Para o padrão MVCS, são utilizados três módulos que servem a cada uma das camadas definidas acima, como já mostrado na Figura~\ref{figura-arq-rails}: para as \textit{views}, utiliza-se o módulo \textbf{\textit{ActionView}}, que provê funcionalidades para a renderização das páginas, construção de formulários, etc. Para os \textit{models}, o módulo \textbf{\textit{ActiveRecord}} garante a associação entre as classes do sistema, bem como o relacionamento da aplicação com o banco de dados. Por fim, mas não menos importante, os \textit{controllers} pertencem ao módulo \textbf{\textit{ActionController}} (ou simplesmente \textit{Controller}), o qual provê um ambiente intermediário entre os \textit{models} e as \textit{views}, garantindo que a informação correta seja exibida ao usuário. Os \textit{Services} do sistema são chamados \textbf{POROs} - \textit{Plain Old Ruby Objects}, que não passam de classes comuns do Ruby com os métodos providos por aquele serviço.





% ========================================================================================
%   Modelos FrameWeb
% ========================================================================================
\section{Modelos FrameWeb}
\label{sec-projeto-modelos-frame-web}

Nesta seção, são exibidos os modelos FrameWeb, citados anteriormente na Seção~\ref{sec-frameweb}. Esses modelos também estão divididos nas camadas da arquitetura do sistema, conforme citado na Seção~\ref{sec-projeto-arquitetura-sistema}.

Apresentamos primeiramente o \textbf{Modelo de Entidades}. Os mapeamentos de persistência são meta-dados das classes de domínio que permitem que os \textit{frameworks} ORM (\textit{Object/Relational Mapping}), \textit{frameworks} que realizam o mapeamento objeto/relacional automaticamente, transformem objetos que estão na memória em linhas de tabelas no banco de dados relacional. Por meio de mecanismos leves de extensão da UML, como estereótipos e restrições, adicionamos tais mapeamentos ao diagrama de classes de domínio, guiando os desenvolvedores na configuração do \textit{framework} ORM. Apesar de tais configurações serem relacionadas mais à persistência do que ao domínio, elas são representadas no Modelo de Entidades porque as classes que são mapeadas e seus atributos são exibidas neste diagrama.~\cite{salvatore2016alocaweb}

As Figuras~\ref{fig-projeto-auth-modelo-entidades}, \ref{fig-projeto-classroom-modelo-entidades}, \ref{fig-projeto-board-modelo-entidades}, \ref{fig-projeto-boardinteractions-modelo-entidades}, \ref{fig-projeto-feedback-modelo-entidades}, \ref{fig-projeto-discussion-modelo-entidades} e \ref{fig-projeto-log-modelo-entidades} mostram o modelo de entidades para os subsistemas \texttt{Auth}, \texttt{Classroom}, \texttt{Board}, \texttt{BoardInteractions}, \texttt{Feedback}, \texttt{Discussion} e \texttt{Log}, respectivamente. A fim de se aproximar da abordagem adotada por bancos de dados, todos os atributos do tipo \texttt{String} tiveram um tamanho (\texttt{size}) atribuído a eles. Da mesma forma, atributos do tipo Data (\texttt{Date}) especificam a granularidade do valor armazenado (\textit{time}, \textit{date} ou \textit{timestamp}), sendo que no último caso, a restrição pode ser omitida. Além disso, diferentemente da abordagem original do FrameWeb proposto em 2007, todos os atributos que são não nulos tiveram a \textit{tag {not null}} omitida, bem como todas as associações foram consideradas do tipo \textit{lazy} devido à forma com que o \textit{Ruby on Rails} carrega os objetos na memória.

Vale ressaltar que todas as classes de domínio estendem de \texttt{ActiveRecord} do \textit{Ruby on Rails}, que é a classe relacionada à persistência de dados do \textit{framework}, sendo que essa herança não é mostrada no diagrama com o intuito de não poluí-lo com várias associações. Além disso, por padrão do framework, todas as classes possuem navegabilidade para os dois lados.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{figuras/projeto/entidades-auth}
	\caption{Modelo de Entidades do subsistema Auth}
	\label{fig-projeto-auth-modelo-entidades}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figuras/projeto/entidades-classroom}
	\caption{Modelo de Entidades do subsistema Classroom}
	\label{fig-projeto-classroom-modelo-entidades}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figuras/projeto/entidades-board}
	\caption{Modelo de Entidades do subsistema Board}
	\label{fig-projeto-board-modelo-entidades}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figuras/projeto/entidades-boardinteractions}
	\caption{Modelo de Entidades do subsistema BoardInteractions}
	\label{fig-projeto-boardinteractions-modelo-entidades}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figuras/projeto/entidades-feedback}
	\caption{Modelo de Entidades do subsistema Feedback}
	\label{fig-projeto-feedback-modelo-entidades}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{figuras/projeto/entidades-discussion}
	\caption{Modelo de Entidades do subsistema Discussion}
	\label{fig-projeto-discussion-modelo-entidades}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{figuras/projeto/entidades-logs}
	\caption{Modelo de Entidades do subsistema Log}
	\label{fig-projeto-log-modelo-entidades}
\end{figure}







Já o \textbf{Modelo de Navegação} é um diagrama de classe da UML que representa os diferentes componentes que formam a camada de Apresentação, como páginas Web, formulários HTML e classes de ação. Esse modelo é utilizado pelos desenvolvedores para guiar a codificação das classes e componentes das camadas de \textbf{\textit{View}} e \textbf{\textit{Controller}}. A classe de ação é o principal componente deste modelo: suas associações de dependência ditam o controle de fluxo quando uma ação é executada.

As funcionalidades criar, editar, excluir e visualizar (abreviadas de CRUD, do inglês \textit{create, read, update e delete}), seguem um mesmo fluxo de execução e de interação com o usuário. Tais funcionalidades são similares para todos os casos de uso cadastrais devido à utilização de \textit{Ruby on Rails}. Esse fluxo de execução similar é representado pela Figura~\ref{fig-projeto-crud-modelo-navegacao} que é um modelo de apresentação genérico. Neste caso, utilizou-se o caso de uso cadastral de \texttt{ForumTopic} a título de exemplo.

\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{figuras/projeto/navegacao-crud}
	\caption{Modelo de Navegação para casos de usos cadastrais.}
	\label{fig-projeto-crud-modelo-navegacao}
\end{figure}

Este é um modelo genérico para todas as entidades que possuem funcionalidades do tipo CRUD. Deve existir, para cada entidade, um \textit{controller} central que fará o controle das requisições executadas pelo usuário. Para interfacear com o usuário, o sistema provê uma tela de listagem de entidades (\textit{/forumtopics}), uma para detalhamento (\textit{/forumtopics/:id}), uma para criação (\textit{/forumtopics/new}) e outra para edição (\textit{/forumtopics/:id/edit}). A partir delas, determinados métodos são invocados no \textit{controller} a fim de realizar a ação desejada e, após isso, uma resposta é retornada ao usuário.

Para os casos de uso que apresentam funções diferentes das CRUDs, o modelo anterior não pode ser aplicado. A Figura~\ref{fig-projeto-modelo-navegacao-auth} apresenta o modelo de navegação para o fluxo de autenticação do sistema, incluindo os casos de uso \textbf{Criar conta}, \textbf{Efetuar login} e \textbf{Obter senha esquecida}.

Nesse diagrama, vale notar a presença da restrição \textbf{result} indicando o tipo de retorno da requisição. Em todos os casos, caso algum erro tenha ocorrido durante o processamento das informações, o usuário retorna para a página anterior a fim de que possa corrigir seu erro. 

Além disso, fica evidente o fluxo de navegação caso as informações estejam corretas: tanto para o login quanto para o reset de senha propriamente dito, o usuário é redirecionado para a lista de turmas. Já no caso do cadastro e da solicitação de uma nova senha, o usuário é redirecionado para a tela de login, onde um aviso deverá informá-lo para que acesse seu e-mail para prosseguir com o processo.

\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{figuras/projeto/navegacao-auth}
	\caption{Modelo de Navegação para o fluxo de autenticação do sistema}
	\label{fig-projeto-modelo-navegacao-auth}
\end{figure}




A Figura~\ref{fig-projeto-modelo-navegacao-download-lote} exibe o modelo de navegação para o caso de uso \textit{Fazer download em lote das submissões de uma tarefa ou prova}. Neste modelo, levou-se em consideração o download em lote das submissões de uma tarefa. Para isso, o professor poderá acessar a página de detalhamento de uma determinada tarefa e, lá, solicitar o download das submissões. Com isso, o método \texttt{download\_submissions} é invocado, retornando para o usuário um arquivo compactado \textbf{.zip} contendo todas as submissões dos alunos daquela turma para aquela determinada tarefa.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{figuras/projeto/navegacao-download-lote}
	\caption{Modelo de Navegação para o caso de uso 'Fazer download em lote das submissões de uma tarefa ou prova'.}
	\label{fig-projeto-modelo-navegacao-download-lote}
\end{figure}


O próximo modelo FrameWeb a ser mostrado é o \textbf{Modelo de Aplicação}, que consiste em um diagrama de classes da UML que representa as classes de serviço, responsáveis pela codificação dos casos de uso, e suas dependências. Esse diagrama é utilizado para guiar a implementação das classes do pacote \textbf{Aplicação} e a configuração das dependências entre os pacotes \textbf{Controle e Aplicação}, ou seja, quais \textit{controllers} dependem de quais classes de serviço~\cite{souza-frameweb07}.


Todas as classes de serviço estendem de \textit{CrudService}, representada na Figura~\ref{figura-frameweb-aplicacao-crudservice} de forma genérica e essa herança não é mostrada no diagrama com o intuito de não polui-lo com várias associações.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.3\textwidth]{figuras/projeto/aplicacao-crudservice.png}
	\caption{Representação da classe \texttt{CrudService} contendo os métodos de CRUD.}
	\label{figura-frameweb-aplicacao-crudservice}
\end{figure}

Embora a arquitetura do \textit{Ruby on Rails} não incorpore injeção de dependências, assume-se que existe uma dependência entre o \textit{controller} e a classe de serviço uma vez que, sem ela, o primeiro deixaria de funcionar.

A seguir, a Figura~\ref{figura-frameweb-aplicacao-auth} representa o modelo de aplicação para o subsistema \textit{Auth}, a Figura~\ref{figura-frameweb-aplicacao-classroom} representa o modelo de aplicação para o subsistema \textit{Classroom}, a Figura~\ref{figura-frameweb-aplicacao-discussion} representa o modelo de aplicação para o subsistema \textit{Discussion}, a Figura~\ref{figura-frameweb-aplicacao-board} representa o modelo de aplicação para o subsistema \textit{Board}, a Figura~\ref{figura-frameweb-aplicacao-boardinteractions} representa o modelo de aplicação para o subsistema \textit{Board Interactions}, a Figura~\ref{figura-frameweb-aplicacao-feedback} representa o modelo de aplicação para o subsistema \textit{Feedback} e a Figura~\ref{figura-frameweb-aplicacao-logs} representa o modelo de aplicação para o subsistema \textit{Logs}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.3\textwidth]{figuras/projeto/aplicacao-auth.png}
	\caption{Modelo de Aplicação para o subsistema Auth.}
	\label{figura-frameweb-aplicacao-auth}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figuras/projeto/aplicacao-classroom.png}
	\caption{Modelo de Aplicação para o subsistema Classroom.}
	\label{figura-frameweb-aplicacao-classroom}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{figuras/projeto/aplicacao-discussion.png}
	\caption{Modelo de Aplicação para o subsistema Discussion.}
	\label{figura-frameweb-aplicacao-discussion}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figuras/projeto/aplicacao-board.png}
	\caption{Modelo de Aplicação para o subsistema Board.}
	\label{figura-frameweb-aplicacao-board}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figuras/projeto/aplicacao-board-interactions.png}
	\caption{Modelo de Aplicação para o subsistema Board Interactions.}
	\label{figura-frameweb-aplicacao-boardinteractions}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{figuras/projeto/aplicacao-feedback.png}
	\caption{Modelo de Aplicação para o subsistema Feedback.}
	\label{figura-frameweb-aplicacao-feedback}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.2\textwidth]{figuras/projeto/aplicacao-logs.png}
	\caption{Modelo de Aplicação para o subsistema Logs.}
	\label{figura-frameweb-aplicacao-logs}
\end{figure}








