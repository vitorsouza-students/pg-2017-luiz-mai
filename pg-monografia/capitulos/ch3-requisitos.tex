%==============================================================================
% TCC - Luiz Felipe Ferreira Mai
% Capítulo 3 - Especificação de Requisitos
%==============================================================================

\chapter{Especificação de Requisitos}
\label{sec-requisitos}


Este capítulo aborda alguns resultados da Engenharia de Requisitos para a construção do sistema Khoeus. Na seção~\ref{sec-requisitos-escopo}, é apresentado o minimundo do projeto; na seção~\ref{sec-requisitos-pacotes}, são apresentados os diagramas de pacotes, na seção~\ref{sec-requisitos-casos-de-uso}, são apresentados diagramas de casos de uso e na seção~\ref{sec-requisitos-diagrama-de-classes}, são apresentados os diagramas de classes. Os requisitos funcionais, requisitos não funcionais e regras de negócio podem ser encontrados no \textbf{Documento de Especificação de Requisitos} que está disponível no Apêndice ao final dessa monografia.


\section{Descrição do Escopo}
\label{sec-requisitos-escopo}

Em um ambiente de aprendizagem como uma escola ou universidade, espera-se que os alunos tenham acesso fácil e direto às informações relativas às aulas, como materiais de aula, notas das atividades realizadas e lista de presença. Para isso, espera-se centralizar essas funcionalidade no formato de turmas, de forma que cada uma delas possua seus alunos, professores, atividades, dentre outros elementos envolvidos no sistema.

\subsection{Organização de turmas}
\label{sec-requisitos-escopo:organizacao-turmas}

A uma turma, estarão vinculados uma série de usuários que poderão exercer papel (\textit{role}) de professor ou de aluno. Para participar de uma turma, um usuário deverá visitá-la e, em seu primeiro acesso, inserir a chave de acesso (que foi definida no momento em que a turma foi criada). Caso a chave esteja correta, o usuário é associado àquela turma e pode acessar seu conteúdo. Além disso, cada turma contém um \textit{board}, que corresponde a todos os itens que serão exibidos aos alunos, como tarefas, arquivos, URLs, dentre outros (descritos na seção \ref{sec-requisitos-escopo:itens-board}). Para melhor organizá-lo, um \textit{board} é subdividido em seções definidas pelo professor e todos os itens deverão estar associados a uma seção. Um \textit{board} contém obrigatoriamente:

\begin{itemize}
	\item Uma \textbf{lista de notícias} que serão criadas exclusivamente pelos professores da turma. As notícias não possuem réplica;
	\item Um \textbf{fórum de discussão} com tópicos que podem ser tanto criados quanto respondidos por professores ou alunos da turma;
	\item Uma \textbf{lista de \textit{LiveQuestions}} com perguntas que os alunos podem realizar durante as aulas;
	\item A \textbf{lista de presença} dos alunos da turma;
	\item O \textbf{livro de notas} contendo a avaliação de cada aluno em cada uma das atividades realizadas.
\end{itemize}

Além disso, o \textit{board} também poderá conter:

\begin{itemize}
	\item \textbf{Arquivos:} permite realizar o download de um arquivo que foi enviado pelo professor;
	\item \textbf{Links:} permite acessar páginas externas;
	\item \textbf{Questionários:} permite responder a perguntas objetivas sem que existam opções corretas, simulando uma enquete;
	\item \textbf{Provas:} permite responder a perguntas objetivas e discursivas que serão avaliadas;
	\item \textbf{Tarefas:}  permite enviar arquivos, textos ou códigos de programação de acordo com a tarefa passada pelo professor;
	\item \textbf{Atividades Externas:}  permite avaliar uma atividade que tenha sido realizada fora da plataforma, como uma atividade feita em sala de aula, por exemplo.
\end{itemize}


\subsection{Controle de usuários}
\label{sec-requisitos-escopo:controle-usuarios}

Todas as funcionalidades do sistema dependem do usuário estar logado em sua conta. Para isso, o Khoeus conta com uma página de login, uma de cadastro e uma de recuperação de senha, permitindo que os usuários possam acessar o sistema. Na página de cadastro, o usuário deverá informar seus dados pessoais (como nome, endereço e telefone), seu e-mail (que será utilizado para realizar o login) e uma senha. Feito isso, um e-mail de confirmação será enviado para o e-mail cadastrado a fim de que o usuário confirme seu cadastro. Na página de login, basta inserir o e-mail e senha para que seja verificada sua autenticidade. Caso estejam corretos, o usuário é redirecionado para a lista de turmas. Caso contrário, deverá inserir novamente seus dados. Na página de recuperação de senha, basta que o usuário digite o e-mail cadastrado em sua conta para que receba um link em sua caixa de entrada permitindo a mudança de senha. 

Um usuário do sistema pode ter permissão de \textbf{Administrador} ou de \textbf{Membro}. Enquanto os membros podem apenas alterar o próprio perfil, ingressar em uma turma ou acessar uma turma já ingressada anteriormente, administradores têm acesso completo ao sistema e podem alterar as configurações gerais do sistema, das turmas e editar o perfil de todos os usuários. Para o sistema, espera-se que seja possível configurar o fuso horário utilizado pelo sistema, definir as informações de SMTP para disparo de e-mails, colocar o sistema em modo de manutenção (acessível apenas para administradores). Para as turmas, espera-se poder definir seu título, sua senha de acesso, definir se a turma possui livro de notas e lista de presença além de ser possível definir as categorias de nota que serão utilizadas para a avaliação dos alunos (mais detalhes na Seção~{\ref{sec-requisitos-escopo:avaliacao}}). Já para os alunos, espera-se ser possível alterar seu perfil (nome, senha, endereço, etc.). Além disso, os administradores têm acesso a um registro com logs de todo o sistema, incluindo todas as ações realizadas pelos usuários, como cadastro, login, criação de itens de board e até mesmo submissão de novas tarefas.

Além das permissões vinculadas à conta, os usuários também possuem um papel (\textit{role}) em cada turma da qual fazem parte, podendo este ser de \textbf{Professor} ou \textbf{Aluno}. Assim, um usuário com a permissão de membro pode assumir a posição de professor ou de aluno dentro de uma turma. Em uma turma, um aluno poderá consultar os itens do \textit{board}, como enviar tarefas e baixar arquivos, consultar as próprias presenças na lista de chamada, consultar suas notas no livro de notas, verificar o calendário de aulas planejadas, criar novas \textit{Live Questions} e enviar mensagens para alunos e professores. Paralelamente a isso, um professor pode inserir novos itens no \textit{board}, lançar presenças e notas dos alunos cadastrados na turma, inserir uma nova aula no calendário, verificar as \textit{Live Questions} vigentes, enviar mensagem para alunos e professores, realizar download em lote dos envios de uma tarefa, prova ou questionário, visualizar o log de atividades dos alunos e definir as configurações da turma, já descritas anteriormente.

Vale ressaltar que um usuário pode ter sua \textit{role} alterada de acordo com a vontade de um administrador. Dessa forma, é possível que um Membro torne-se Administrador (ou vice-versa) e que um Aluno vire Professor (ou vice-versa).


\subsection{Itens do Board}
\label{sec-requisitos-escopo:itens-board}

Itens do \textit{board} estão sempre associados a uma seção com exceção do fórum de discussões, das notícias, das \textit{Live Questions}, da lista de presença e do livro de notas, que são fixados no topo do \textit{board}. Cada item do \textit{board} possui uma ação diferente e possui campos diferentes no momento da inclusão.

\subsubsection{Arquivo}
\label{sec-requisitos-escopo:itens-board:arquivo}
Ao acessar um arquivo, o aluno realiza o download do mesmo. Para criar um novo arquivo, o professor deverá informar seu nome, a descrição e deverá fazer o upload a partir dos arquivos de seu computador.

\subsubsection{Link}
\label{sec-requisitos-escopo:itens-board:link}
Ao acessar um link, o aluno é redirecionado para a URL correspondente. Para inserir um novo link no board, o professor deverá informar o título do link, sua descrição e a sua respectiva URL.

\subsubsection{Notícias}
\label{sec-requisitos-escopo:itens-board:noticias}
No topo do board, o aluno pode acessar a lista de notícias relacionada àquela turma e onde estão todas as notícias já criadas pelos professores da turma. Para inserir uma nova noticia, o professor deverá informar apenas o título e o conteúdo desejado.

\subsubsection{Fórum de Discussão}
\label{sec-requisitos-escopo:itens-board:forum}
No fórum de discussão, tanto alunos quanto professores podem criar novos tópicos e, da mesma forma, responder tópicos já criados anteriormente. Para criar um novo tópico, espera-se que o usuário insira um título e seu conteúdo. No caso da réplica de um tópico, basta seu conteúdo.

\subsubsection{\textit{Live Questions}}
\label{sec-requisitos-escopo:itens-board:live-questions}
Na lista de \textit{Live Questions}, alunos poderão criar novas dúvidas e professores poderão visualizá-las por até 24h após sua criação. A ideia é que as dúvidas sejam criadas e sanadas no momento da aula. Na sua criação, o aluno deve inserir sua dúvida e informar se deseja ser tratado como Anônimo ou não. 

\subsubsection{Questionário}
\label{sec-requisitos-escopo:itens-board:questionario}
Os questionários simulam, basicamente, uma enquete ou uma pesquisa de opinião: no momento da criação, o professor define uma série de perguntas (todas de múltipla escolha) e as respostas possíveis para cada uma delas, sendo possível marcar apenas uma das alternativas. Nesse item, não existe resposta correta e, por isso, os questionários devem ser utilizados para recolher a opinião dos alunos sobre algum assunto determinado. Ao acessar um questionário, os alunos visualizarão as perguntas que foram criadas pelo professor e deverão respondê-las, não sendo possível responder a um questionário mais de uma vez.  Questionários possuem uma data para iniciar e um tempo-limite para que possam ser respondidos. Após isso, é possível coletar os resultados do questionário. 

\subsubsection{Prova}
\label{sec-requisitos-escopo:itens-board:prova}
Uma prova, como o próprio nome já diz, é elegível para ser uma forma de avaliação do aluno. Dessa forma, no momento da criação, o professor deve associá-la a uma categoria de nota e informar um título e uma descrição. Provas possuem uma data para iniciar e uma data de término que também deverão ser informadas. Além disso, o professor deve inserir quais perguntas haverá na prova, o tipo da questão (discursiva ou objetiva) e qual a nota máxima para cada uma das questões (totalizando 100 pontos). No caso de perguntas objetivas, deve-se informar qual das alternativas é a correta. Após encerrado o prazo da prova, os professores poderão visualizar as respostas dos alunos para cada uma das questões e atribuir a elas uma nota variando de 0 à nota máxima para aquela questão, sendo possível também informar um feedback para cada questão.

\subsubsection{Tarefa}
\label{sec-requisitos-escopo:itens-board:tarefa}
Tarefas representam a principal forma de avaliar um aluno, uma vez que permitem o envio de textos, arquivos ou até mesmo códigos de programas. No momento da criação da tarefa, o professor deverá informar o tipo de tarefa, a que categoria de nota esta pertence, título, descrição, data de início e data limite de envio. Uma vez finalizada, o professor poderá avaliar a tarefa de cada aluno concedendo-o uma nota de 0 a 100 e sendo possível ainda dar um feedback em forma de texto ou comentar linhas do código enviado pelo aluno.

A forma de submissão da tarefa varia de acordo com o seu tipo: caso seja do tipo texto, o aluno deverá preencher um campo de texto com o que foi pedido na descrição da tarefa. Caso seja do tipo arquivo, o aluno deverá fazer o upload do arquivo que foi requisitado. No caso da tarefa ser do tipo código, ele deverá inseri-lo no campo de texto, podendo realizar a compilação em tempo real a fim de verificar a corretude de seu código. 

\subsubsection{Atividade Externa}
\label{sec-requisitos-escopo:itens-board:ativida-externa}
Atividades externas representam exercícios ou provas que foram feitas em sala e que, por isso, não são avaliados diretamente no Khoeus. Dessa forma, alunos não precisarão submeter quaisquer informações neste tipo de item (uma vez que esta foi feita presencialmente), mas o professor poderá lançar as notas de cada um dos alunos e incluir um pequeno feedback a respeito da atividade. 

Atividades externas também devem estar vinculadas a uma categoria de nota.

\subsection{Avaliação}
\label{sec-requisitos-escopo:avaliacao}
Dentro das configurações de uma turma, é possível definir a forma com que a média final é calculada e quais as condições de aprovação dos alunos. Dessa forma, professores podem criar categorias de nota e associar a cada uma delas um peso no cálculo da média final. É possível criar, por exemplo, as categorias de nota ``Prova'' e ``Trabalho'' e definir a média final como 60\% da nota de prova somado a 40\% da nota de trabalho.

Para que a média seja calculada corretamente, o professor deverá definir no momento da criação de uma \textbf{Tarefa}, \textbf{Prova} ou \textbf{Atividade Externa} a que categoria de nota ela pertence.

%%% Início de seção. %%%
\section{Diagrama de Pacotes}
\label{sec-requisitos-pacotes}

Dada a descrição do minimundo, foi possível estruturar o sistema em sete  pacotes, como pode ser visto na Figura~\ref{fig-requisitos-diagrama-pacotes}. A descrição de cada um dos subsistemas está contida na Tabela~\ref{tabela-subsistema}.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figuras/requisitos/subsistemas}
	\caption{Diagrama de pacotes para o sistema Khoeus}
	\label{fig-requisitos-diagrama-pacotes}
\end{figure}


\begin{table}[h]
	\centering	
	\vspace{0.5cm}
	\caption{Descrição dos subsistemas}
	\label{tabela-subsistema}
	\begin{tabular}{|p{4cm}|p{11cm}|}  \hline \rowcolor[rgb]{0.8,0.8,0.8}
		
		Subsistema & Descrição \\\hline 
		
		Classroom & Subsistema contendo as funcionalidades relacionadas diretamente às turmas.  \\\hline
		
		Board & Envolve todas as funcionalidades relativas ao gerenciamento dos itens do board. \\\hline
		
		Board Interactions & Corresponde a todas as interações que os usuários podem realizar com os itens do board, como submeter tarefas ou realizar provas. \\\hline
		
		Feedback & Permite a avaliação de atividades e a visualização das notas do aluno em uma turma. \\\hline
		
		Discussions & Subsistema contendo as funcionalidades relativas ao fórum de discussão, às notícias e às \textit{Live Questions}. \\\hline
		
		Logs & Este sistema consiste na visualização de logs gerados a partir das atividades dos usuários do sistema. \\\hline  
		
	\end{tabular}	
\end{table}


%%% Início de seção. %%%
\section{Diagrama de Casos de Uso}
\label{sec-requisitos-casos-de-uso}
	
Como visto na Seção~\ref{sec-requisitos-pacotes}, este projeto foi divido em sete subsistemas. Na Subseção~\ref{sec-requisitos-casos-de-uso-classroom} os casos de uso do subsistema \texttt{Classroom}, na Subseção~\ref{sec-requisitos-casos-de-uso-board} os casos de uso do subsistema \texttt{Board}, na Subseção~\ref{sec-requisitos-casos-de-uso-boardinteractions} os casos de uso do subsistema \texttt{Board Interactions}, na Subseção~\ref{sec-requisitos-casos-de-uso-feedback} os casos de uso do subsistema \texttt{Feedback}, na Subseção~\ref{sec-requisitos-casos-de-uso-discussion} os casos de uso do subsistema \texttt{Discussion} e na Subseção~\ref{sec-requisitos-casos-de-uso-log} os casos de uso do subsistema \texttt{Log}.

\subsection{Atores x Casos de Uso}
\label{sec-requisitos-casos-de-uso-atores}
O modelo de casos de uso visa capturar e descrever as funcionalidades que um sistema deve prover para os atores que interagem com o mesmo. No contexto deste projeto, temos basicamente dois tipos de atores: aqueles relacionados ao papel do usuário no sistema (Visitante, Administrador ou Membro) e aqueles relacionados ao papel do usuário dentro de uma turma (Professor ou Aluno).

Um Membro pode ser um professor ou aluno e, por isso, utilizou-se um mecanismo de controle de acesso baseado em \textit{roles} (papéis) a fim de evitar que alunos possam, por exemplo, configurar turmas ou criar itens do board. Dessa forma, cada membro do sistema possui um papel associado a ele dependendo da turma em que está inscrito.

Maiores informações e detalhes sobre os casos de uso poderão ser consultados no \textbf{Documento de Análise de Requisitos} que está disponível no Apêndice ao final dessa monografia.


\subsection{Subsistema Classroom}
\label{sec-requisitos-casos-de-uso-classroom}

A Figura~\ref{fig-requisitos-classroom-diagrama-casos-uso} mostra os casos de uso do subsistema \textbf{\texttt{Classroom}} que serão descritos a seguir. Esta divisão do sistema foi criada para prover funcionalidades relacionadas diretamente às turmas, como é o caso dos casos de uso \textbf{Configurar turma}, \textbf{Ingressar em uma turma}, \textbf{Definir roles de usuários na turma} e \textbf{Gerenciar turmas}, sendo esse último um caso de uso cadastral, assegurando as operações básicas de CRUD.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figuras/requisitos/casos-de-uso-classroom}
	\caption{Diagrama de Casos de Uso para o subsistema Classroom.}
	\label{fig-requisitos-classroom-diagrama-casos-uso}
\end{figure}

Praticamente todas as funcionalidades do sistema giram em torno da inscrição dos usuários em determinadas turmas e, por isso, o caso de uso para \textbf{Ingressar em uma turma} espera que um usuário tenha acesso ao conteúdo da turma desejada após inserida a senha estipulada no momento de sua criação. Criação esta que está contida no caso de uso cadastral \textbf{Gerenciar turmas} que fornece as operações básicas de CRUD para essa classe do sistema. Criada uma turma, professores também podem alterar suas configurações por meio do caso de uso \textbf{Configurar turma}. Uma vez inscrito em uma turma, um usuário pode assumir os papéis de aluno ou professor. Esse controle do papel de um usuário é gerenciado pelo caso de uso \textbf{Definir roles de usuários na turma}. 

Além disso, esse subsistema também provê as funcionalidades básicas de uma turma e que não dependem de seu board. De um lado, professores podem \textbf{Gerenciar eventos no calendário}, adicionando-os na data prevista e lançar a presença dos alunos nas aulas definidas no calendário. Do outro lado, alunos podem consultar suas presenças ao longo das aulas e os eventos registrados no calendário. Para facilitar a comunicação entre membros de uma mesma turma, o caso de uso \textbf{Enviar mensagens} permite que alunos e professores troquem mensagens entre eles.

Por fim, o caso de uso \textbf{Definir configurações do sistema} permite que administradores definam diversas variáveis que irão reger o sistema, como definições de fuso-horário, definições SMTP e modo de manutenção.


\subsection{Subsistema Board}
\label{sec-requisitos-casos-de-uso-board}

A Figura~\ref{fig-requisitos-board-diagrama-casos-uso} mostra os casos de uso do subsistema \textbf{\texttt{Board}} que serão descritos a seguir. Esta divisão do sistema foi criada para prover o gerenciamento dos itens do board de uma turma por meio dos casos de uso \textbf{Gerenciar seções do board}, \textbf{Gerenciar arquivos do board}, \textbf{Gerenciar links do board}, \textbf{Gerenciar questionários do board}, \textbf{Gerenciar provas do board}, \textbf{Gerenciar tarefas do board} e \textbf{Gerenciar atividades externas do board}.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figuras/requisitos/casos-de-uso-board}
	\caption{Diagrama de Casos de Uso para o subsistema Board.}
	\label{fig-requisitos-board-diagrama-casos-uso}
\end{figure}

Para cada caso de uso listado, espera-se que um professor possa criar, alterar, consultar e excluir a classe em questão, passando as informações relativas a ela no momento de sua criação ou alteração. Além disso, com exceção do caso de uso \textbf{Gerenciar seções do board}, deve-se informar a seção a que cada item do board pertence no momento de sua criação.

\subsection{Subsistema Board Interactions}
\label{sec-requisitos-casos-de-uso-boardinteractions}

A Figura~\ref{fig-requisitos-boardinteractions-diagrama-casos-uso} mostra os casos de uso do subsistema \textbf{\texttt{Board Interactions}} que serão descritos a seguir. Esta divisão do sistema foi criada para prover as funcionalidades em que alunos possam interagir com os itens de board criados pelo subsistema \texttt{\textbf{Board}}.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figuras/requisitos/casos-de-uso-board-interactions}
	\caption{Diagrama de Casos de Uso para o subsistema Board Interactions.}
	\label{fig-requisitos-boardinteractions-diagrama-casos-uso}
\end{figure}

Nesse subsistema, cada tipo de item de board possui uma interação diferente com o usuário. Para o caso de uso \textbf{Acessar um link}, o usuário simplesmente é redirecionado para a página em questão e para o caso de uso \textbf{Fazer download de arquivo}, inicia-se o download do arquivo que foi inserido no sistema.

Para os casos de uso \textbf{Responder a um questionário} e \textbf{Resolver uma prova}, o usuário deverá responder a uma série de questões (objetivas no caso de questionários e objetivas ou discursivas no caso de provas), submetendo suas respostas ao final do processo. Para \textbf{Submeter uma tarefa}, o aluno poderá submeter tarefas que sejam no formato texto, arquivo ou código.

Professores poderão \textbf{Visualizar resultado de questionários} uma vez que tenha se passado a data limite para respondê-lo, sendo possível visualizar seus resultados de forma resumida por meio de porcentagens, gráficos, taxas de resposta etc.


\subsection{Subsistema Feedback}
\label{sec-requisitos-casos-de-uso-feedback}

A Figura~\ref{fig-requisitos-feedback-diagrama-casos-uso} mostra os casos de uso do subsistema \textbf{\texttt{Feedback}} que serão descritos a seguir. Esta divisão do sistema foi criada para permitir que professores avaliem as provas, tarefas e atividades externas dos alunos e para que estes possam consultar suas notas e o feedback atribuído às suas atividades.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figuras/requisitos/casos-de-uso-feedback}
	\caption{Diagrama de Casos de Uso para o subsistema Feedback.}
	\label{fig-requisitos-feedback-diagrama-casos-uso}
\end{figure}

Nesse subsistema, tem-se dois tipos diferentes de casos de usos: (i) casos de uso de professores para avaliação de atividades. Para facilitar o processo de avaliação, é possível \textbf{Fazer download em lote das submissões de uma tarefa ou prova} por meio de um arquivo compactado. Além disso, é possível \textbf{Avaliar tarefas, provas ou atividades externas individualmente}, concedendo uma nota e opcionalmente um feedback para cada um dos alunos ou \textbf{Avaliar tarefas, provas ou atividades externas em lote} por meio do \textit{upload} de uma planilha contendo as notas e feedbacks de todos os alunos daquela turma; (ii) casos de uso de alunos para consulta de notas de forma que eles possam \textbf{Visualizar resultado de provas}, \textbf{Visualizar nota de atividade externa} e \textbf{Visualizar feedback de tarefa}. Além disso, o aluno tem a sua disposição um livro de notas onde pode \textbf{Consultas suas notas} ao longo do curso.

\subsection{Subsistema Discussion}
\label{sec-requisitos-casos-de-uso-discussion}

A Figura~\ref{fig-requisitos-discussion-diagrama-casos-uso} mostra os casos de uso do subsistema \textbf{\texttt{Discussion}} que serão descritos a seguir. Esta divisão do sistema foi criada para permitir que professores e alunos troquem informações por meio do fórum, de notícias e de \textit{Live Questions}.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figuras/requisitos/casos-de-uso-discussion}
	\caption{Diagrama de Casos de Uso para o subsistema Discussion.}
	\label{fig-requisitos-discussion-diagrama-casos-uso}
\end{figure}

O fórum de discussão tem o intuito de ser um ambiente livre para que alunos e professores possam discutir temas de seu interesse e, por isso, ambos podem \textbf{Criar tópico no fórum de discussão}, \textbf{Responder a um tópico no fórum de discussão} e \textbf{Visualizar os tópicos do fórum de discussão}. No caso dos dois primeiros, basta que o usuário digite o conteúdo da mensagem e, no caso da criação de tópicos, fornecer um título. Para manter o controle das publicações, professores podem \textbf{Gerenciar tópico do fórum} de forma que podem realizar as operações básicas de CRUD sobre eles quando julgarem necessário.

As notícias de uma turma dizem respeito exclusivamente a informações que os professores desejam passar para seus alunos e, por isso, apenas professores podem \textbf{Gerenciar notícias}, podendo criar, alterar, consultar e excluí-las quando julgar necessário. Embora não possam criá-las, alunos podem \textbf{Visualizar as notícias da turma}.

As \textit{Live Questions} devem ser usadas para realizar perguntas para os professores no momento da aula. O fluxo consiste no fato do aluno \textbf{Adicionar nova Live Question} enquanto o professor poderá \textbf{Visualizar Live Questions}. Como o intuito é de que essas perguntas sejam feitas em momento de aula, elas serão excluídas passadas 24 horas de sua criação, conforme Figura~\ref{fig-requisitos-discussion-diagrama-estado}.

\begin{figure}[h]
	\centering
	\includegraphics{figuras/requisitos/diagrama-estado-livequestion}
	\caption{Diagrama de Estados para uma LiveQuestion.}
	\label{fig-requisitos-discussion-diagrama-estado}
\end{figure}

\subsection{Subsistema Log}
\label{sec-requisitos-casos-de-uso-log}

A Figura~\ref{fig-requisitos-log-diagrama-casos-uso} mostra os casos de uso do subsistema \textbf{\texttt{Log}} que serão descritos a seguir. Esta divisão do sistema foi criada para garantir um controle do que foi feito dentro do sistema e quem o fez.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{figuras/requisitos/casos-de-uso-log}
	\caption{Diagrama de Casos de Uso para o subsistema Log.}
	\label{fig-requisitos-log-diagrama-casos-uso}
\end{figure}

Basicamente, o sistema possui duas categorias de log: como o papel de um professor está restrito à sua turma, só é possível \textbf{Visualiar logs dos alunos} daquela turma em que estão inseridos. Paralelamente a isso, administradores podem \textbf{Visualiar todos os logs do sistema} a fim de garantir uma espécie de ``auditoria'', podendo tomar atitudes caso sejam necessárias.






%%% Início de seção. %%%
\section{Diagrama de Classes}
\label{sec-requisitos-diagrama-de-classes}


Assim como os casos de uso na Seção~\ref{sec-requisitos-casos-de-uso}, os diagramas de classes estão divididos de acordo com a divisão dos subsistemas. Na Subseção~\ref{sec-requisitos-diagrama-de-classes-classroom} estão as classes pertecentes ao subsistema \texttt{Classroom}, na Subseção~\ref{sec-requisitos-diagrama-de-classes-board} estão as classes pertecentes ao subsistema \texttt{Board}, na Subseção~\ref{sec-requisitos-diagrama-de-classes-boardinteractions} estão as classes pertecentes ao subsistema \texttt{Board Interactions}, na Subseção~\ref{sec-requisitos-diagrama-de-classes-feedback} estão as classes pertecentes ao subsistema \texttt{Feedback}, na Subseção~\ref{sec-requisitos-diagrama-de-classes-discussion} estão as classes pertecentes ao subsistema \texttt{Discussion} e na Subseção~\ref{sec-requisitos-diagrama-de-classes-log} estão as classes pertecentes ao subsistema \texttt{Log}. 

 Vale ressaltar que algumas associações são consideradas obrigatórias nos dois sentidos pois considera-se que o tempo decorrido entre a criação de ambas é extremamente pequeno. Além disso, por estarmos considerando \textbf{entidades}, uma só faria sentido quando estivesse associado à outra.


\subsection{Subsistema Classroom}
\label{sec-requisitos-diagrama-de-classes-classroom}

A Figura~\ref{fig-requisitos-classroom-diagrama-classes} exibe o diagrama de classes do subsistema \textbf{\texttt{Classroom}}. 

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/requisitos/diagrama-classe-classroom}
	\caption{Diagrama de Classes do subsistema Classroom.}
	\label{fig-requisitos-classroom-diagrama-classes}
\end{figure}

Aqui, fica evidente a relação de \texttt{User} com \texttt{Classroom} por meio da classe \texttt{Subscription}: cada usuário poderá estar inscrito em uma turma, assumindo um determinado papel (\textit{role}). Dentro de uma turma, existe um calendário com os eventos (\texttt{Events}) previstos e as aulas (\texttt{Class}) que foram ou serão lecionadas, sendo que pra cada delas o professor poderá lançar a presença ou falta de cada aluno por meio da classe \texttt{Presence}.

Aqui, vale notar que as turmas podem não estar associadas a nenhuma presença (por meio das aulas) ou categorias de nota, já que o administrador terá a opção de definir sua existência por meio dos atributos \texttt{has\_grades} e \texttt{has\_attendance}.

Para que seja possível verificar se alunos foram aprovados ou reprovados, cada turma terá um conjunto de \texttt{Grade Category} que especificará a forma com que a média final será calculada. Além disso, a classe \texttt{Message} permite que alunos possam trocar mensagens entre si ou com os professores daquela turma.

Por fim, a fim de garantir a consistência do sistema, a classe \texttt{Configuration} permite que administradores realizem as configurações necessárias para seu bom funcionamento, como configurações para envio de e-mail e idioma de preferência.



\subsection{Subsistema Board}
\label{sec-requisitos-diagrama-de-classes-board}

A Figura~\ref{fig-requisitos-board-diagrama-classes} exibe o diagrama de classes do subsistema \textbf{\texttt{Board}}. 

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/requisitos/diagrama-classe-board}
	\caption{Diagrama de Classes do subsistema Board.}
	\label{fig-requisitos-board-diagrama-classes}
\end{figure}

Nesse subsistema estão contidas as classes relacionadas aos diversos itens do \textit{board} da turma. Para melhor organização, todos os \texttt{BoardItens} estão organizados em \texttt{Sections}, agrupando itens relacionados entre si. \texttt{BoardItens} podem ser de 6 tipos diferentes: \texttt{Link}, \texttt{File} (Arquivo), \texttt{Survey} (Questionário), \texttt{Test} (Prova), \texttt{Assignment} (Tarefa) e \texttt{External Activity} (Atividade Externa). Aqui, fica evidente que os itens do board precisam estar, necessariamente, associados a uma seção da turma.

\texttt{Surveys} conterão exclusivamente perguntas objetivas e, por isso, possuem associadas a ela, estão as \texttt{Survey Questions} que correspondem a uma pergunta de determinado questionário. Associada a estas últimas estão as \texttt{Survey Answers}, que representam as possíveis escolhas para aquela questão.

Da mesma forma, \texttt{Tests} possuem associados a eles uma série de \texttt{Test Questions} que podem ser discursivas ou objetivas (determinadas pelo atributo \texttt{type}). No caso das questões objetivas, a questão possui um conjunto de \texttt{Test Alternatives} associadas a ela. A alternativa correta da questão é definida pelo atributo \texttt{correct}.


\subsection{Subsistema Board Interactions}
\label{sec-requisitos-diagrama-de-classes-boardinteractions}

A Figura~\ref{fig-requisitos-boardinteractions-diagrama-classes} exibe o diagrama de classes do subsistema \textbf{\texttt{Board Interactions}}. 

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/requisitos/diagrama-classe-board-interactions}
	\caption{Diagrama de Classes do subsistema Board Interactions.}
	\label{fig-requisitos-boardinteractions-diagrama-classes}
\end{figure}

De forma complementar ao \textbf{Board}, esse subsistema representa as interações dos alunos com os itens do board. As classes \texttt{Survey Response} e \texttt{Test AlternativeResponse} armazenam a escolha de uma alternativa das questões objetivas de questionários e provas. Além disso, a classe \texttt{Test TextResponse} armazena a resposta de um aluno para questões discursivas, bem como a nota atribuída pelo professor.

Tarefas podem ser submetidas por meio de textos, arquivos ou códigos. \texttt{Text Submission}, \texttt{File Submission} e \texttt{Code Submission} representam as submissões do aluno para cada um desses tipos de tarefa. No caso das submissões de código, este é armazenado linha a linha por meio da classe \texttt{Code Line}.

Por fim, a classe \texttt{Activity} representa a atividade externa desenvolvida por um aluno e a nota atribuída a ela.


\subsection{Subsistema Feedback}
\label{sec-requisitos-diagrama-de-classes-feedback}

A Figura~\ref{fig-requisitos-feedback-diagrama-classes} exibe o diagrama de classes do subsistema \textbf{\texttt{Feedback}}. 

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/requisitos/diagrama-classe-feedback}
	\caption{Diagrama de Classes do subsistema Feedback.}
	\label{fig-requisitos-feedback-diagrama-classes}
\end{figure}

A fim de permitir que professores avaliem as atividades de um aluno e deem um feedback a eles, esse subsistema reúne as classes \texttt{Text Feedback} e \texttt{Code Line Feedback}. A primeira delas permite que professores atribuam um texto avaliando a prova, tarefa ou atividade externa do aluno enquanto a última permite que o professor atribua comentários para linhas de código da tarefa de um aluno.


\subsection{Subsistema Discussion}
\label{sec-requisitos-diagrama-de-classes-discussion}

A Figura~\ref{fig-requisitos-discussion-diagrama-classes} exibe o diagrama de classes do subsistema \textbf{\texttt{Discussion}}. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{figuras/requisitos/diagrama-classe-discussion}
	\caption{Diagrama de Classes do subsistema Discussion.}
	\label{fig-requisitos-discussion-diagrama-classes}
\end{figure}

Dentro de cada turma existe um ambiente de discussão formado por um fórum de discussões, uma lista de notícia e o grupo de \textit{Live Questions}. Um fórum de discussões é formado por \texttt{Forum Topics} que podem ser criados e respondidos tanto por professores quanto alunos. As réplicas de um tópico são representadas por \texttt{Forum Reply}. Já a lista de notícias é exclusividade dos professores e cada uma é representada pela classe \texttt{News}. Por fim, as \texttt{Live Questions} podem ser criadas por alunos e visualizadas por professores.

\subsection{Subsistema Log}
\label{sec-requisitos-diagrama-de-classes-log}

A Figura~\ref{fig-requisitos-log-diagrama-classes} exibe o diagrama de classes do subsistema \textbf{\texttt{Log}}. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{figuras/requisitos/diagrama-classe-log}
	\caption{Diagrama de Classes do subsistema Log.}
	\label{fig-requisitos-log-diagrama-classes}
\end{figure}

Nesse subsistema, vale notar que os \texttt{Logs} estão associados a uma conta a fim de indicar as ações que um determinado usuário realizou dentro do sistema.