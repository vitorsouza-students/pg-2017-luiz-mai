
\chapter{Descrição do Minimundo}
\label{sec-minimundo}

Em um ambiente de aprendizagem como uma escola ou universidade, espera-se que os alunos tenham acesso fácil e direto às informações relativas às aulas, como materiais de aula, notas das atividades realizadas e lista de presença. Para isso, espera-se centralizar essas funcionalidade no formato de turmas, de forma que cada uma delas possua seus alunos, professores, atividades, dentre outros elementos envolvidos no sistema.

\section{Organização de turmas}
\label{sec-minimundo:organizacao-turmas}

A uma turma, estarão vinculados uma série de usuários que poderão exercer papel (\textit{role}) de professor ou de aluno. Para participar de uma turma, um usuário deverá visitá-la e, em seu primeiro acesso, inserir a chave de acesso (que foi definida no momento em que a turma foi criada). Caso a chave esteja correta, o usuário é associado àquela turma e pode acessar seu conteúdo. Além disso, cada turma contém um \textit{board}, que corresponde a todos os itens que serão exibidos aos alunos, como tarefas, arquivos, URLs, dentre outros (descritos na seção \ref{sec-minimundo:itens-board}). Para melhor organizá-lo, um \textit{board} é subdividido em seções definidas pelo professor e todos os itens deverão estar associados a uma seção. Um \textit{board} contém obrigatoriamente:

\begin{itemize}
	\item Uma \textbf{lista de notícias} que serão criadas exclusivamente pelos professores da turma. As notícias não possuem réplica.
	\item Um \textbf{fórum de discussão} com tópicos que podem ser tanto criados quanto respondidos por professores ou alunos da turma.
	\item Uma \textbf{lista de \textit{LiveQuestions}} com perguntas que os alunos podem realizar durante as aulas
	\item A \textbf{lista de presença} dos alunos da turma
	\item O \textbf{livro de notas} contendo a avaliação de cada aluno em cada uma das atividades realizadas.
\end{itemize}

Além disso, o \textit{board} também poderá conter:

\begin{itemize}
	\item \textbf{Arquivos:} permite realizar o download de um arquivo que foi enviado pelo professor;
	\item \textbf{Links:} permite acessar páginas externas;
	\item \textbf{Questionários:} permite responder a perguntas objetivas sem que existam opções corretas, simulando uma enquete.
	\item \textbf{Provas:} permite responder a perguntas objetivas e discursivas que serão avaliadas.
	\item \textbf{Tarefas:}  permite enviar arquivos, textos ou códigos de programação de acordo com a tarefa passada pelo professor.
	\item \textbf{Atividades Externas:}  permite avaliar uma atividade que tenha sido realizada fora da plataforma, como uma atividade feita em sala de aula, por exemplo.
\end{itemize}


\section{Controle de usuários}
\label{sec-minimundo:controle-usuarios}

Todas as funcionalidades do sistema dependem do usuário estar logado em sua conta. Para isso, o Khoeus conta com uma página de login, uma de cadastro e uma de recuperação de senha, permitindo que os usuários possam acessar o sistema. Na página de cadastro, o usuário deverá informar seus dados pessoais (como nome, endereço e telefone), seu e-mail (que será utilizado para realizar o login) e uma senha. Feito isso, um e-mail de confirmação será enviado para o e-mail cadastrado a fim de que o usuário confirme seu cadastro. Na página de login, basta inserir o email e senha para que seja verificada sua autenticidade. Caso estejam corretos, o usuário é redirecionado para a lista de turmas. Caso contrário, deverá inserir novamente seus dados. Na página de recuperação de senha, basta que o usuário digite o e-mail cadastrado em sua conta para que receba um link em sua caixa de entrada permitindo a mudança de senha. 

Um usuário do sistema pode ter permissão de \textbf{Administrador} ou de \textbf{Membro}. Enquanto os membros podem apenas alterar o próprio perfil, ingressar em uma turma ou acessar uma turma já ingressada anteriormente, administradores têm acesso completo ao sistema e podem alterar as configurações gerais do sistema, das turmas e editar o perfil de todos os usuários. Para o sistema, espera-se que seja possível configurar o fuso horário utilizado pelo sistema, definir as informações de SMTP para disparo de e-mails, colocar o sistema em modo de manutenção (acessível apenas para administradores). Para as turmas, espera-se poder definir seu título, sua senha de acesso, definir se a turma possui livro de notas e lista de presença além de ser possível definir as categorias de nota que serão utilizadas para a avaliação dos alunos (mais detalhes na Seção~{\ref{sec-minimundo:avaliacao}}). Já para os alunos, espera-se ser possível alterar seu perfil (nome, senha, endereço, etc.). Além disso, os administradores têm acesso a um registro com logs de todo o sistema, incluindo todas as ações realizadas pelos usuários, como cadastro, login, criação de itens de board e até mesmo submissão de novas tarefas.

Além das permissões vinculadas à conta, os usuários também possuem um papel (\textit{role}) em cada turma da qual fazem parte, podendo este ser de \textbf{Professor} ou \textbf{Aluno}. Assim, um usuário com a permissão de membro pode assumir a posição de professor ou de aluno dentro de uma turma. Em uma turma, um aluno poderá consultar os itens do \textit{board}, como enviar tarefas e baixar arquivos, consultar as próprias presenças na lista de chamada, consultar suas notas no livro de notas, verificar o calendário de aulas planejadas, criar novas \textit{Live Questions} e enviar mensagens para alunos e professores. Paralelamente a isso, um professor pode inserir novos itens no \textit{board}, lançar presenças e notas dos alunos cadastrados na turma, inserir uma nova aula no calendário, verificar as \textit{Live Questions} vigentes, enviar mensagem para alunos e professores, realizar download em lote dos envios de uma tarefa, prova ou questionário, visualizar o log de atividades dos alunos e definir as configurações da turma, já descritas anteriormente.

Vale ressaltar que um usuário pode ter sua \textit{role} alterada de acordo com a vontade de um administrador. Dessa forma, é possível que um Membro torne-se Administrador (ou vice-versa) e que um Aluno vire Professor (ou vice-versa).


\section{Itens do Board}
\label{sec-minimundo:itens-board}

Itens do \textit{board} estão sempre associados a uma seção com exceção do fórum de discussões, das notícias, das \textit{Live Questions}, da lista de presença e do livro de notas, que são fixados no topo do \textit{board}. Cada item do \textit{board} possui uma ação diferente e possui campos diferentes no momento da inclusão.

\subsection{Arquivo}
\label{sec-minimundo:itens-board:arquivo}
Ao acessar um arquivo, o aluno realiza o download do mesmo. Para criar um novo arquivo, o professor deverá informar seu nome, a descrição e deverá fazer o upload a partir dos arquivos de seu computador.

\subsection{Link}
\label{sec-minimundo:itens-board:link}
Ao acessar um link, o aluno é redirecionado para a URL correspondente. Para inserir um novo link no board, o professor deverá informar o título do link, sua descrição e a sua respectiva URL.

\subsection{Notícias}
\label{sec-minimundo:itens-board:noticias}
No topo do board, o aluno pode acessar a lista de notícias relacionada àquela turma e onde estão todas as notícias já criadas pelos professores da turma. Para inserir uma nova noticia, o professor deverá informar apenas o título e o conteúdo desejado.

\subsection{Fórum de Discussão}
\label{sec-minimundo:itens-board:forum}
No fórum de discussão, tanto alunos quanto professores podem criar novos tópicos e, da mesma forma, responder tópicos já criados anteriormente. Para criar um novo tópico, espera-se que o usuário insira um título e seu conteúdo. No caso da réplica de um tópico, basta seu conteúdo.

\subsection{\textit{Live Questions}}
\label{sec-minimundo:itens-board:live-questions}
Na lista de \textit{Live Questions}, alunos poderão criar novas dúvidas e professores poderão visualizá-las por até 24h após sua criação. A ideia é que as dúvidas sejam criadas e sanadas no momento da aula. Na sua criação, o aluno deve inserir sua dúvida e informar se deseja ser tratado como Anônimo ou não. 

\subsection{Questionário}
\label{sec-minimundo:itens-board:questionario}
Os questionários simulam, basicamente, uma enquete ou uma pesquisa de opinião: no momento da criação, o professor define uma série de perguntas (todas de múltipla escolha) e as respostas possíveis para cada uma delas, sendo possível marcar apenas uma das alternativas. Nesse item, não existe resposta correta e, por isso, os questionários devem ser utilizados para recolher a opinião dos alunos sobre algum assunto determinado. Ao acessar um questionário, os alunos visualizarão as perguntas que foram criadas pelo professor e deverão respondê-las, não sendo possível responder a um questionário mais de uma vez.  Questionários possuem uma data para iniciar e um tempo-limite para que possam ser respondidos. Após isso, é possível coletar os resultados do questionário. 

\subsection{Prova}
\label{sec-minimundo:itens-board:prova}
Uma prova, como o próprio nome já diz, é elegível para ser uma forma de avaliação do aluno. Dessa forma, no momento da criação, o professor deve associá-la a uma categoria de nota e informar um título e uma descrição. Provas possuem uma data para iniciar e uma data de término que também deverão ser informadas. Além disso, o professor deve inserir quais perguntas haverão na prova, o tipo da questão (discursiva ou objetiva) e qual a nota máxima para cada uma das questões (totalizando 100 pontos). No caso de perguntas objetivas, deve-se informar qual das alternativas é a correta. Após encerrado o prazo da prova, os professores poderão visualizar as respostas dos alunos para cada uma das questões e atribuir a elas uma nota variando de 0 à nota máxima para aquela questão, sendo possível também informar um feedback para cada questão.

\subsection{Tarefa}
\label{sec-minimundo:itens-board:tarefa}
Tarefas representam a principal forma de avaliar um aluno, uma vez que permitem o envio de textos, arquivos ou até mesmo códigos de programas. No momento da criação da tarefa, o professor deverá informar o tipo de tarefa, a que categoria de nota esta pertence, título, descrição, data de início e data limite de envio. Uma vez finalizada, o professor poderá avaliar a tarefa de cada aluno concedendo-o uma nota de 0 a 100 e sendo possível ainda dar um feedback em forma de texto ou comentar linhas do código enviado pelo aluno.

A forma de submissão da tarefa varia de acordo com o seu tipo: caso seja do tipo texto, o aluno deverá preencher um campo de texto com o que foi pedido na descrição da tarefa. Caso seja do tipo arquivo, o aluno deverá fazer o upload do arquivo que foi requisitado. No caso da tarefa ser do tipo código, ele deverá inserí-lo no campo de texto, podendo realizar a compilação em tempo real a fim de verificar a corretude de seu código. 

\subsection{Atividade Externa}
\label{sec-minimundo:itens-board:ativida-externa}
Atividades externas representam exercícios ou provas que foram feitas em sala e que, por isso, não são avaliados diretamente no Khoeus. Dessa forma, alunos não precisarão submeter quaisquer informações neste tipo de item (uma vez que esta foi feita presencialmente), mas o professor poderá lançar as notas de cada um dos alunos e incluir um pequeno feedback a respeito da atividade. 

Atividades externas também devem estar vinculadas a uma categoria de nota.

\section{Avaliação}
\label{sec-minimundo:avaliacao}
Dentro das configurações de uma turma, é possível definir a forma com que a média final é calculada e quais as condições de aprovação dos alunos. Dessa forma, professores podem criar categorias de nota e associar a cada uma delas um peso no cálculo da média final. É possível criar, por exemplo, as categorias de nota ``Prova'' e ``Trabalho'' e definir a média final como 60\% da nota de prova somado a 40\% da nota de trabalho.

Para que a média seja calculada corretamente, o professor deverá definir no momento da criação de uma \textbf{Tarefa}, \textbf{Prova} ou \textbf{Atividade Externa} a que categoria de nota ela pertence.

