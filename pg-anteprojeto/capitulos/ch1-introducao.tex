\chapter{Introdução}
\label{chap-intro}
Vivemos em uma época em que a computação tem atuado cada vez mais nas diversas áreas do conhecimento e, obviamente, também passou a atuar na Educação: manter planilhas de notas, listas de presenças e tirar dúvidas de alunos via e-mail são, sem dúvidas, atividades exaustivas e, muitas vezes, burocráticas. Com o advento de sistemas computacionais, os ambientes de apoio ao aprendizado se popularizaram entre as instituições de ensino superior por remover parte do esforço exigido de um professor e por facilitar o acesso do aluno às informações que deseja.

No entanto, sabe-se que a grande maioria dos sistemas desse tipo disponíveis tentam ser o mais genéricos possíveis a fim de atender às mais diversas áreas, o que acaba deixando-os com uma usabilidade ruim. Além disso, poucos são os sistemas que atendem a cursos com foco na programação de computadores. Os que existem são, em sua maioria, pagos, inviabilizando sua utilização.

Dessa forma, o intuito deste projeto é o de desenvolver um ambiente de apoio ao aprendizado aberto, de fácil usabilidade e que permita sua utilização por professores e alunos da área de computação.

\section{Motivação e Justificativa}
\label{sec-motivacao}

A possibilidade de um ambiente em que professores possam disponibilizar materiais de aula e requisitar tarefas dos alunos é bastante atraente para ambas as partes e, por isso, a utilização de plataformas de aprendizado tem se tornado cada vez maior em ambientes de ensino como escolas, faculdades ou cursos externos. No entanto, numa tentativa de atender a todos, as plataformas mais utilizadas atualmente acabam se tornando confusas e fornecendo ferramentas que, muitas vezes, não são utilizadas.

Não obstante a vasta gama de funcionalidades fornecidas por essas plataformas, nenhuma delas lida com códigos de programação de forma eficiente: em sua maioria, códigos são gerenciados como arquivos ou textos comuns, impedindo que seja possível explorar ao máximo o aprendizado do aluno na área de programação.

Dessa forma, o Khoeus vem com o intuito de facilitar o processo de aprendizado de alunos de Computação e, ao mesmo tempo, tentar corrigir as falhas e os excessos das plataformas já existentes de forma a ser o mais simples e prático possível, agregando a possibilidade de trabalhar com códigos de programação.

\section{Objetivos}
\label{sec-objetivos}

Tem-se como objetivo principal deste trabalho aplicar o conhecimento adquirido em diversas disciplinas que foram cursadas ao longo do curso de graduação em Engenharia de Computação a fim de se desenvolver um sistema Web a ser apresentado para uma banca de professores como requisito primordial para a conclusão do curso. Dentre tais disciplinas, podemos citar Programação III, Engenharia de Software, Banco de Dados,  Desenvolvimento Web e Web Semântica, entre outras.
Além disso, o trabalho servirá para auxiliar na avaliação do método FrameWeb, proposto em~\cite{souza-frameweb07} e atualizado em~\cite{martins-mestrado15}. Tal método será utilizado na fase de projeto do sistema.

\section{Descrição}
\label{sec-descricao}

Em um ambiente de aprendizagem como uma escola ou universidade, espera-se que os alunos tenham acesso fácil e direto às informações relativas às aulas, como materiais de aula, notas das atividades realizadas e lista de presença. Para isso, espera-se centralizar essas funcionalidade no formato de turmas, de forma que cada uma delas possua seus alunos, professores, atividades, dentre outros elementos envolvidos no sistema.

A uma turma, estarão vinculados uma série de usuários que poderão exercer papel (\textit{role}) de professor ou de aluno. Além disso, cada turma contém um \textit{board} dividido em seções, que corresponde a todos os itens que serão exibidos aos alunos, como tarefas, arquivos, URLs, dentre outros. Um \textit{board} contém obrigatoriamente:

\begin{itemize}
	\item Uma \textbf{lista de notícias} que serão criadas exclusivamente pelos professores da turma. As notícias não possuem réplica;
	\item Um \textbf{fórum de discussão} com tópicos que podem ser tanto criados quanto respondidos por professores ou alunos da turma;
	\item Uma \textbf{lista de \textit{LiveQuestions}} com perguntas que os alunos podem realizar durante as aulas;
	\item A \textbf{lista de presença} dos alunos da turma;
	\item O \textbf{livro de notas} contendo a avaliação de cada aluno em cada uma das atividades realizadas.
\end{itemize}

Além disso, o \textit{board} também poderá conter:

\begin{itemize}
	\item \textbf{Arquivos:} permite realizar o download de um arquivo que foi enviado pelo professor;
	\item \textbf{Links:} permite acessar páginas externas;
	\item \textbf{Questionários:} permite responder a perguntas objetivas sem que existam opções corretas, simulando uma enquete;
	\item \textbf{Provas:} permite responder a perguntas objetivas e discursivas que serão avaliadas;
	\item \textbf{Tarefas:}  permite enviar arquivos, textos ou códigos de programação de acordo com a tarefa passada pelo professor;
	\item \textbf{Atividades Externas:}  permite avaliar uma atividade que tenha sido realizada fora da plataforma, como uma atividade feita em sala de aula, por exemplo.
\end{itemize}

Para permitir uma melhor gerência da avaliação dos alunos de uma turma, é possível definir a forma com que a média final é calculada e quais as condições de aprovação dos alunos. Dessa forma, professores podem criar categorias de nota e associar a cada uma delas um peso no cálculo da média final. É possível criar, por exemplo, as categorias de nota ``Prova'' e ``Trabalho'' e definir a média final como 60\% da nota de prova somado a 40\% da nota de trabalho. Para que a média seja calculada corretamente, o professor deverá definir no momento da criação de uma \textbf{Tarefa}, \textbf{Prova} ou \textbf{Atividade Externa} a que categoria de nota ela pertence.

\section{Metodologia}
\label{sec-metodologia}

As seguinte atividades são realizadas no contexto deste trabalho:

\begin{itemize}
	\item Estudo nas áreas de Engenharia de Software, Engenharia de Requisitos, Desenvolvimento de Softwares e Desenvolvimento Web;
	\item Estudo de \textit{frameworks} Web existentes seguido da escolha do mais adequado;
	\item Elaboração da documentação de requisitos;
	\item Elaboração da documentação de projeto;
	\item Codificação e desenvolvimento do sistema Web;
	\item Testes e ajustes;
	\item Redação da monografia e apresentação dos resultados.
\end{itemize}

\section{Resultados Esperados}
\label{sec-resultados-esperados}

Neste trabalho, espera-se desenvolver uma plataforma capaz de prover um ambiente de aprendizado simples e acessível a alunos dos mais diversos níveis de ensino, permitindo, também, um fácil controle por parte de professores das mais diversas áreas de ensino. Embora o foco esteja no ensino da programação, faz-se possível utilizar a plataforma para qualquer outra área do conhecimento.

\section{Cronograma}
\label{sec-cronograma}

As tabelas~\ref{tab:cronograma-1} e~\ref{tab:cronograma-2} apresentam o cronograma deste trabalho, referindo-se às atividades abaixo listadas por número.

\begin{enumerate}
	\item Estudo nas áreas de Engenharia de Software, Engenharia de Requisitos, Desenvolvimento de Softwares e Desenvolvimento Web;
	\item Estudo de \textit{frameworks} Web existentes seguido da escolha do mais adequado;
	\item Elaboração da documentação de requisitos;
	\item Elaboração da documentação de projeto;
	\item Codificação e desenvolvimento do sistema Web;
	\item Testes e ajustes;
	\item Redação da monografia e apresentação dos resultados.
\end{enumerate}

\begin{table}[htb]
	\centering
	\caption{Cronograma de Atividades}
	\label{tab:cronograma-1}
	\resizebox{\columnwidth}{!}{
		\begin{tabular}{c|c|c|c|c|c|c}
			Atividade & Janeiro/17 & Fevereiro/17  & Março/17  & Abril/17 & Maio/17 & Junho/17\\ \hline
			1&     X      &  	  X   	       &  	X	  	   & 		X	&     X   &      X    \\ \hline
			2&            &  	   	     	   &  	  X  	   & 		X	&         &           \\ \hline
			3&            &  		           &  	  X 	   & 		X   &   X     &     X      \\ \hline
			4&            &  			       &  			   & 	        &         &     X      \\ \hline
			5&            &  			       &  			   & 	        &    X    &   X       \\ \hline
			6&            &  			       &  			   & 	        &         &           \\ \hline
			7&            &  			       &  			   & 	        &         &           \\ \hline
		\end{tabular}
	}
\end{table}

\begin{table}[htb]
	\centering
	\caption{Cronograma de Atividades}
	\label{tab:cronograma-2}
	\resizebox{\columnwidth}{!}{
		\begin{tabular}{c|c|c|c|c|c|c}
			Atividade & Julho/17 & Agosto/17  & Setembro/17  & Outubro/17 & Novembro/17 & Dezembro/17\\ \hline
			1&            &  	      	       &  	 	  	   & 		 	&         &           \\ \hline'
			2&            &  	   	     	   &  	     	   & 		 	&         &           \\ \hline
			3&            &  		           &  	    	   & 		    &         &            \\ \hline
			4&    X       &  		X	       &  			   & 	        &         &            \\ \hline
			5&    X       &  		X          &  	X   	   & 	 X      &         &           \\ \hline
			6&            &  			       &  			   & 	 X      &    X    &           \\ \hline
			7&            &  			       &  			   & 	 X      &    X    &    X      \\ \hline
		\end{tabular}
	}
\end{table}

\section{Recursos Necessários}
\label{sec-recursos-necessarios}

Ao longo do trabalho, foram (ou serão) utilizados:

\begin{itemize}
	\item Computador com acesso à Internet;
	\item Ruby on Rails (RoR): \textit{framework} para desenvolvimento Web baseado na linguagem de programação Ruby;
	\item PostgreSQL: SGDB objeto-relacional;
	\item RubyMine: IDE para Ruby / RoR;
	\item GitHub: repositório de código;
	\item Heroku: \textit{deploy} e monitoramento da aplicação;
	\item Astah: desenvolvimento de diagramas.
\end{itemize}